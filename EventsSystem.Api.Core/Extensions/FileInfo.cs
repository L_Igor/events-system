﻿namespace EventsSystem.Api.Core.Extensions
{
    public class FileInfo
    {
        public FileInfo(long length, string fileName, string contentType, string tempFileName)
        {
            Length = length;
            FileName = fileName;
            ContentType = contentType;
            TempFileName = tempFileName;
        }

        /// <summary>
        /// Gets the raw Content-Type header of the uploaded file.
        /// </summary>
        public string ContentType { get; }

        /// <summary>
        /// Gets the file length in bytes.
        /// </summary>
        public long Length { get; }

        /// <summary>
        /// Gets the file name from the Content-Disposition header.
        /// </summary>
        public string FileName { get; }

        /// <summary>
        /// Gets the full path of the temporary file.
        /// </summary>
        public string TempFileName { get; }
    }
}