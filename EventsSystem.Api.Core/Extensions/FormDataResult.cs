﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace EventsSystem.Api.Core.Extensions
{
    public class FormDataResult
    {
        public FormValueProvider FormValueProvider { get; set; }

        public IEnumerable<FileInfo> Files { get; set; }
    }
}