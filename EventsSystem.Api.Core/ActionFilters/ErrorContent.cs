﻿using System.Collections.Generic;

namespace EventsSystem.Api.Core.ActionFilters
{
    public class ErrorContent
    {
        public bool HasErrors { get; set; }

        public IEnumerable<Error> Errors { get; set; }
    }

    public class Error
    {
        public string FieldName { get; set; }

        public IEnumerable<ErrorMessage> ErrorMessages { get; set; }
    }

    public class ErrorMessage
    {
        public string Message { get; set; }
    }
}
