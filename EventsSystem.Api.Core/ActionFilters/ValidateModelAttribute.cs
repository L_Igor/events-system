﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace EventsSystem.Api.Core.ActionFilters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var modelState = filterContext.ModelState;

            if (modelState.IsValid)
            {
                return;
            }

            var errors = modelState.Select(s => new Error()
            {
                FieldName = s.Key,
                ErrorMessages = s.Value.Errors.Select(x => new ErrorMessage()
                {
                    Message = x.ErrorMessage,
                }),
            });

            var errorObject = new ErrorContent()
            {
                HasErrors = true,
                Errors = errors,
            };

            filterContext.Result = new BadRequestObjectResult(errorObject);
        }
    }
}