﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace EventsSystem.Api.Core.Authentication
{
    public class ExtensionAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public ExtensionAuthenticationMiddleware(RequestDelegate next, IAuthenticationSchemeProvider schemes)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }
            if (schemes == null)
            {
                throw new ArgumentNullException(nameof(schemes));
            }

            _next = next;
            Schemes = schemes;
        }

        public IAuthenticationSchemeProvider Schemes { get; set; }

        public async Task Invoke(HttpContext context)
        {
            var authorizationHeader = context.Request.Headers[HeaderNames.Authorization].FirstOrDefault();
            var authenticateScheme =
                authorizationHeader?.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)
                    .FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(authenticateScheme))
            {
                var schemes = await Schemes.GetAllSchemesAsync();

                var oldAuthenticateScheme = authenticateScheme;

                authenticateScheme =
                    schemes.FirstOrDefault(p =>
                            p.Name.Equals(authenticateScheme, StringComparison.InvariantCultureIgnoreCase))
                        ?.Name;

                context.Request.Headers.Remove(HeaderNames.Authorization);
                authorizationHeader = authorizationHeader.Replace(oldAuthenticateScheme, authenticateScheme);
                context.Request.Headers.Add(HeaderNames.Authorization, authorizationHeader);

                if (authenticateScheme != null)
                {
                    var result = await context.AuthenticateAsync(authenticateScheme);
                    if (result?.Principal != null)
                    {
                        context.User = result.Principal;
                    }
                }
            }

            await _next(context);
        }
    }
}