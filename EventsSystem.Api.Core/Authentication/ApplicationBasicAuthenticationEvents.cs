﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;

namespace EventsSystem.Api.Core.Authentication
{
    public class ApplicationBasicAuthenticationEvents : BasicAuthenticationEvents
    {
        public delegate bool ValidateCredentialsDelegate(string userName, string password);

        public ApplicationBasicAuthenticationEvents()
        {
        }

        public ApplicationBasicAuthenticationEvents(ValidateCredentialsDelegate onValidateCredentials)
        {
            OnValidateCredentials = onValidateCredentials;
        }

        public ValidateCredentialsDelegate OnValidateCredentials { get; set; }

        public override Task ValidatePrincipalAsync(ValidatePrincipalContext context)
        {
            if (OnValidateCredentials == null)
            {
                throw new Exception();
            }

            if (OnValidateCredentials(context.UserName, context.Password))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                };

                var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, BasicAuthenticationDefaults.AuthenticationScheme));
                context.Principal = principal;
            }

            return Task.CompletedTask;
        }
    }
}