﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Net.Http.Headers;

namespace EventsSystem.Api.Core.Authentication
{
    public class ApplicationAuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public ApplicationAuthenticationMiddleware(RequestDelegate next, IAuthenticationSchemeProvider schemes)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }
            if (schemes == null)
            {
                throw new ArgumentNullException(nameof(schemes));
            }

            _next = next;
            Schemes = schemes;
        }

        public IAuthenticationSchemeProvider Schemes { get; set; }

        public async Task Invoke(HttpContext context)
        {
            context.Features.Set<IAuthenticationFeature>(new AuthenticationFeature
            {
                OriginalPath = context.Request.Path,
                OriginalPathBase = context.Request.PathBase
            });

            // Give any IAuthenticationRequestHandler schemes a chance to handle the request
            var handlers = context.RequestServices.GetRequiredService<IAuthenticationHandlerProvider>();
            foreach (var scheme in await Schemes.GetRequestHandlerSchemesAsync())
            {
                var handler = await handlers.GetHandlerAsync(context, scheme.Name) as IAuthenticationRequestHandler;
                if (handler != null && await handler.HandleRequestAsync())
                {
                    return;
                }
            }

            var authorizationHeader = context.Request.Headers[HeaderNames.Authorization].FirstOrDefault();
            var authenticateScheme =
                authorizationHeader?.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries)
                    .FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(authenticateScheme))
            {
                var schemes = await Schemes.GetAllSchemesAsync();

                var oldAuthenticateScheme = authenticateScheme;

                authenticateScheme =
                    schemes.FirstOrDefault(p =>
                            p.Name.Equals(authenticateScheme, StringComparison.InvariantCultureIgnoreCase))
                        ?.Name;

                context.Request.Headers.Remove(HeaderNames.Authorization);
                authorizationHeader = authorizationHeader.Replace(oldAuthenticateScheme, authenticateScheme);
                context.Request.Headers.Add(HeaderNames.Authorization, authorizationHeader);
            }

            if (String.IsNullOrWhiteSpace(authenticateScheme))
            {
                var defaultAuthenticate = await Schemes.GetDefaultAuthenticateSchemeAsync();
                authenticateScheme = defaultAuthenticate?.Name;
            }

            if (!String.IsNullOrWhiteSpace(authenticateScheme))
            {
                var result = await context.AuthenticateAsync(authenticateScheme);
                if (result?.Principal != null)
                {
                    context.User = result.Principal;
                }
            }

            await _next(context);
        }
    }
}