﻿using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="ILectureRatingRepository"/>
    /// </summary>
    public class LectureRatingRepository : IntExtendedRepository<LectureRating>, ILectureRatingRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LectureRatingRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public LectureRatingRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
