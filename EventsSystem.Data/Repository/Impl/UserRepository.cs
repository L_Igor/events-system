﻿using System.Linq;
using System.Threading.Tasks;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IUserRepository"/>
    /// </summary>
    public class UserRepository : IntExtendedRepository<User>, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public UserRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }

        /// <summary>
        /// Gets the entity by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Entity from repository</returns>
        public User GetByEmail(string email)
        {
            return
                base.Query()
                    .FirstOrDefault(p => p.Email.Equals(email/*, StringComparison.InvariantCultureIgnoreCase*/));
        }

        /// <summary>
        /// Asynchronously gets the entity by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Entity from repository</returns>
        public async Task<User> GetByEmailAsync(string email)
        {
            return await Task.Run(() => GetByEmail(email));
        }

        /// <summary>
        /// Gets the entity by username.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns>Entity from repository</returns>
        public User GetByName(string userName)
        {
            return
                base.Query()
                    .FirstOrDefault(p => p.UserName.Equals(userName/*, StringComparison.InvariantCultureIgnoreCase*/));
        }

        /// <summary>
        /// Asynchronously gets the entity by username.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns>Entity from repository</returns>
        public async Task<User> GetByNameAsync(string userName)
        {
            return await Task.Run(() => GetByName(userName));
        }
    }
}
