﻿using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IStorageRepository"/>
    /// </summary>
    public class StorageRepository : IntExtendedRepository<Storage>, IStorageRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StorageRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public StorageRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
