﻿using System.Linq;
using System.Threading.Tasks;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IRefreshTokenRepository"/>
    /// </summary>
    public class RefreshTokenRepository : IntExtendedRepository<RefreshToken>, IRefreshTokenRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RefreshTokenRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public RefreshTokenRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }

        /// <summary>
        /// Gets the entity by token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Entity from repository</returns>
        public RefreshToken GetByToken(string token)
        {
            return
                base.Query()
                    .FirstOrDefault(p => p.Token.Equals(token/*, StringComparison.InvariantCultureIgnoreCase*/));
        }

        /// <summary>
        /// Asynchronously gets the entity by token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Entity from repository</returns>
        public async Task<RefreshToken> GetByTokenAsync(string token)
        {
            return await Task.Run(() => GetByToken(token));
        }
    }
}
