﻿using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IQARepository"/>
    /// </summary>
    public class QARepository : IntExtendedRepository<QA>, IQARepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QARepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public QARepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
