﻿using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="ILectureRepository"/>
    /// </summary>
    public class LectureRepository : IntExtendedRepository<Lecture>, ILectureRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LectureRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public LectureRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
