﻿using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IEventRepository"/>
    /// </summary>
    public class EventRepository : IntExtendedRepository<Event>, IEventRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventRepository"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public EventRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
