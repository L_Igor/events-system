﻿using EventsSystem.Data.Infrastructure;
using EventsSystem.Data.Infrastructure.Impl;

namespace EventsSystem.Data.Repository.Impl
{
    /// <summary>
    /// The extended base repository with integer primary key
    /// </summary>
    public class IntExtendedRepository<T> : ExtendedBaseRepository<T, int>, IIntExtendedRepository<T>
        where T : class, IEntity<int>, ITimestampEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntExtendedRepository{T}"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public IntExtendedRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }
    }
}
