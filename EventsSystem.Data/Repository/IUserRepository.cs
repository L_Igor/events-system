﻿using System.Threading.Tasks;
using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="User"/> entity
    /// </summary>
    public interface IUserRepository : IIntExtendedRepository<User>
    {
        /// <summary>
        /// Gets the entity by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Entity from repository</returns>
        User GetByEmail(string email);

        /// <summary>
        /// Asynchronously gets the entity by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>Entity from repository</returns>
        Task<User> GetByEmailAsync(string email);

        /// <summary>
        /// Gets the entity by username.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns>Entity from repository</returns>
        User GetByName(string userName);

        /// <summary>
        /// Asynchronously gets the entity by username.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns>Entity from repository</returns>
        Task<User> GetByNameAsync(string userName);
    }
}
