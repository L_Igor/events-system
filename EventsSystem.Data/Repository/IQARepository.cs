﻿using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="QA"/> entity
    /// </summary>
    public interface IQARepository : IIntExtendedRepository<QA>
    {
    }
}
