﻿using EventsSystem.Data.Infrastructure;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Provides basic interface for entity extended repositories with integer primary key
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    public interface IIntExtendedRepository<T> : IExtendedBaseRepository<T, int>
        where T : class, IEntity<int>, ITimestampEntity
    {
        
    }
}