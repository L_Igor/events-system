﻿using System.Threading.Tasks;
using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="RefreshToken"/> entity
    /// </summary>
    public interface IRefreshTokenRepository : IIntExtendedRepository<RefreshToken>
    {
        /// <summary>
        /// Gets the entity by token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Entity from repository</returns>
        RefreshToken GetByToken(string token);

        /// <summary>
        /// Asynchronously gets the entity by token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns>Entity from repository</returns>
        Task<RefreshToken> GetByTokenAsync(string token);
    }
}
