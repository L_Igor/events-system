﻿using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Storage"/> entity
    /// </summary>
    public interface IStorageRepository : IIntExtendedRepository<Storage>
    {
    }
}
