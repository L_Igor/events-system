﻿using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Event"/> entity
    /// </summary>
    public interface IEventRepository : IIntExtendedRepository<Event>
    {
    }
}
