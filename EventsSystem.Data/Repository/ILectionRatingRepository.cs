﻿using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="LectureRating"/> entity
    /// </summary>
    public interface ILectureRatingRepository : IIntExtendedRepository<LectureRating>
    {
    }
}
