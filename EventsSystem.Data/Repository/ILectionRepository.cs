﻿using EventsSystem.Data.Entities;

namespace EventsSystem.Data.Repository
{
    /// <summary>
    /// Repository interface for <see cref="Lecture"/> entity
    /// </summary>
    public interface ILectureRepository : IIntExtendedRepository<Lecture>
    {
    }
}
