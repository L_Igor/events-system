﻿using System;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("Lectures")]
    public class Lecture : Entity<int>
    {
        [Column, NotNull, DataType("nvarchar(256)")]
        public string Name { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Description { get; set; }

        [Column, NotNull]
        public DateTimeOffset StartDateTime { get; set; }

        [Column, Nullable]
        public DateTimeOffset EndDateTime { get; set; }

        [Column, NotNull]
        public float Rating { get; set; }

        [Column, NotNull]
        public long SumRating { get; set; }

        [Column, NotNull]
        public int CountRatings { get; set; }

        [Column, NotNull]
        public int CountQuestions { get; set; }

        [Column, NotNull]
        public int EventId { get; set; }

        [Column, NotNull]
        public int UserId { get; set; }



        [Association(ThisKey = nameof(EventId), OtherKey = nameof(Entities.Event.Id), CanBeNull = false)]
        public virtual Event Event { get; set; }

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(Entities.User.Id), CanBeNull = false)]
        public virtual User User { get; set; }
    }
}
