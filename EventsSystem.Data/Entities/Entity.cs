﻿using System;
using EventsSystem.Data.Infrastructure;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    public class Entity<TKey> : IEntity<TKey>, ITimestampEntity
    {
        [PrimaryKey, Identity]
        public virtual TKey Id { get; set; }

        [Column, NotNull]
        public virtual DateTime CreatedAtDateTimeUtc { get; set; }

        [Column, Nullable]
        public virtual DateTime? UpdatedAtDateTimeUtc { get; set; }

        [Column, Nullable]
        public virtual DateTime? DeletedAtDateTimeUtc { get; set; }
    }
}
