﻿using System;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("Events")]
    public class Event : Entity<int>
    {
        [Column, NotNull, DataType("nvarchar(256)")]
        public string Name { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Summary { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Location { get; set; }

        [Column, NotNull]
        public DateTimeOffset StartDateTime { get; set; }

        [Column, NotNull]
        public DateTimeOffset EndDateTime { get; set; }
    }
}
