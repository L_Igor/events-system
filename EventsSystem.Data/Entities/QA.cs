﻿using System;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("QAs")]
    public class QA : Entity<int>
    {
        [Column, NotNull, DataType("nvarchar(256)")]
        public string Question { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Answer { get; set; }

        [Column, NotNull]
        public DateTimeOffset QuestionDateTime { get; set; }

        [Column, Nullable]
        public DateTimeOffset? AnswerDateTime { get; set; }

        [Column, NotNull]
        public int AskingId { get; set; }

        [Column, Nullable]
        public int? AnsweringId { get; set; }

        [Column, NotNull]
        public int LectureId { get; set; }



        [Association(ThisKey = nameof(AskingId), OtherKey = nameof(User.Id), CanBeNull = false)]
        public virtual User Asking { get; set; }

        [Association(ThisKey = nameof(AnsweringId), OtherKey = nameof(User.Id), CanBeNull = true)]
        public virtual User Answering { get; set; }

        [Association(ThisKey = nameof(LectureId), OtherKey = nameof(Entities.Lecture.Id), CanBeNull = false)]
        public virtual Lecture Lecture { get; set; }
    }
}
