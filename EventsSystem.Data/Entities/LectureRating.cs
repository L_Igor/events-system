﻿using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("LectureRatings")]
    public class LectureRating : Entity<int>
    {
        [Column, NotNull]
        public int Rating { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Advantages { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Disadvantages { get; set; }

        [Column, Nullable, DataType("nvarchar(512)")]
        public string AdditionalInfo { get; set; }

        [Column, NotNull]
        public int LectureId { get; set; }

        [Column, NotNull]
        public int UserId { get; set; }



        [Association(ThisKey = nameof(LectureId), OtherKey = nameof(Entities.Lecture.Id), CanBeNull = false)]
        public virtual Lecture Lecture { get; set; }

        [Association(ThisKey = nameof(UserId), OtherKey = nameof(Entities.User.Id), CanBeNull = false)]
        public virtual User User { get; set; }
    }
}
