﻿using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("Storage")]
    public class Storage : Entity<int>
    {
        [Column, Nullable, DataType("varbinary(max)")]
        public byte[] Data { get; set; }

        [Column, Nullable, DataType("varchar(64)")]
        public string MimeType { get; set; }

        [Column, Nullable]
        public long Length { get; set; }

        [Column, Nullable, DataType("varchar(128)")]
        public string OriginalFileName { get; set; }
    }
}
