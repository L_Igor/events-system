﻿using System;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("RefreshTokens")]
    public class RefreshToken : Entity<int>
    {
        [Column, NotNull, DataType("varchar(128)")]
        public string Token { get; set; }

        [Column, NotNull]
        public DateTime Expires { get; set; }

        [Column, NotNull]
        public DateTime CreatedAt { get; set; }

        [Column, NotNull]
        public int UserId { get; set; }



        [Association(ThisKey = nameof(UserId), OtherKey = nameof(Entities.User.Id), CanBeNull = false)]
        public User User { get; set; }
    }
}
