﻿using System.Collections.Generic;
using LinqToDB.Mapping;

namespace EventsSystem.Data.Entities
{
    [Table("Users")]
    public class User : Entity<int>
    {
        [Column, NotNull, DataType("nvarchar(256)")]
        public string Email { get; set; }

        [Column, NotNull, DataType("nvarchar(256)")]
        public string UserName { get; set; }

        [Column, Nullable, DataType("varchar(512)")]
        public string PasswordHash { get; set; }

        [Column, Nullable, DataType("varchar(128)")]
        public string Salt { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string FullName { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Bio { get; set; }

        [Column, Nullable, DataType("nvarchar(64)")]
        public string Occupation { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Photo { get; set; }

        [Column, Nullable]
        public int? PhotoId { get; set; }

        [Column, Nullable, DataType("nvarchar(256)")]
        public string Statistics { get; set; }



        [Association(ThisKey = nameof(Id), OtherKey = nameof(Lecture.UserId), CanBeNull = true)]
        public virtual ICollection<Lecture> Lectures { get; set; }
    }
}
