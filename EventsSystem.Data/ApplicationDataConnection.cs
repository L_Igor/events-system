﻿using System;
using System.Collections.Generic;
using System.Text;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Settings;
using LinqToDB;
using LinqToDB.Configuration;
using LinqToDB.Data;

namespace EventsSystem.Data
{
    public class ApplicationDataConnection : DataConnection
    {
        //public ApplicationDataConnection(DatabaseSettings dbSettings)
        //{
        //    DefaultSettings = dbSettings;
        //}

        public ITable<Event> Events => GetTable<Event>();

        public ITable<Lecture> Lectures => GetTable<Lecture>();

        public ITable<LectureRating> LectureRatings => GetTable<LectureRating>();

        public ITable<QA> QAs => GetTable<QA>();

        public ITable<Storage> Storage => GetTable<Storage>();

        public ITable<User> Users => GetTable<User>();
    }
}
