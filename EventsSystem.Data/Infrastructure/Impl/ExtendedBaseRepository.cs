﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace EventsSystem.Data.Infrastructure.Impl
{
    /// <summary>
    /// The extended base repository
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    /// <typeparam name="TKey">Primary key type</typeparam>
    public class ExtendedBaseRepository<T, TKey> : BaseRepository<T>, IExtendedBaseRepository<T, TKey>
        where T : class, IEntity<TKey>, ITimestampEntity
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedBaseRepository{T, TKey}"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        public ExtendedBaseRepository(IContextFactory contextFactory)
            : base(contextFactory)
        {
        }

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Found entity or null</returns>
        public override T GetById(object id)
        {
            //todo: Find the best implementation

            var entity = base.GetById(id);

            if (entity == null || entity.DeletedAtDateTimeUtc.HasValue)
            {
                return null;
            }

            return entity;
        }

        /// <summary>
        /// Asynchronously gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Found entity or null</returns>
        public override async Task<T> GetByIdAsync(object id)
        {
            //todo: Find the best implementation

            var entity = await base.GetByIdAsync(id);

            if (entity == null || entity.DeletedAtDateTimeUtc.HasValue)
            {
                return null;
            }

            return entity;
        }

        /// <summary>
		/// Adds the specified entity into the repository.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <returns>
		/// Current repository instance for chained operations
		/// </returns>
		public override IRepository<T> Add(T entity)
        {
            entity.CreatedAtDateTimeUtc = DateTime.UtcNow;
            base.Add(entity);
            return this;
        }

        /// <summary>
        /// Asynchronously adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public override async Task<IRepository<T>> AddAsync(T entity)
        {
            entity.CreatedAtDateTimeUtc = DateTime.UtcNow;
            await base.AddAsync(entity);
            return this;
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public override IRepository<T> Update(T entity)
        {
            var exsistingEntity = base.GetById(entity.Id);
            entity.CreatedAtDateTimeUtc = exsistingEntity.CreatedAtDateTimeUtc;
            entity.UpdatedAtDateTimeUtc = DateTime.UtcNow;
            entity.DeletedAtDateTimeUtc = exsistingEntity.DeletedAtDateTimeUtc;
            base.Update(entity);
            return this;
        }

        /// <summary>
        /// Asynchronously updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public override async Task<IRepository<T>> UpdateAsync(T entity)
        {
            var exsistingEntity = await base.GetByIdAsync(entity.Id);
            entity.CreatedAtDateTimeUtc = exsistingEntity.CreatedAtDateTimeUtc;
            entity.UpdatedAtDateTimeUtc = DateTime.UtcNow;
            entity.DeletedAtDateTimeUtc = exsistingEntity.DeletedAtDateTimeUtc;
            await base.UpdateAsync(entity);
            return this;
        }

        /// <summary>
        /// Deletes the specified entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IExtendedBaseRepository<T, TKey> Delete(TKey id, bool isSoftDelete = true)
        {
            var entity = base.GetById(id);

            if (isSoftDelete)
            {
                entity.DeletedAtDateTimeUtc = DateTime.UtcNow;
                base.Update(entity);
            }
            else
            {
                base.Delete(entity);
            }

            return this;
        }

        /// <summary>
        /// Asynchronously deletes the specified entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public async Task<IExtendedBaseRepository<T, TKey>> DeleteAsync(TKey id, bool isSoftDelete = true)
        {
            var entity = await base.GetByIdAsync(id);

            if (isSoftDelete)
            {
                entity.DeletedAtDateTimeUtc = DateTime.UtcNow;
                await base.UpdateAsync(entity);
            }
            else
            {
                await base.DeleteAsync(entity);
            }

            return this;
        }

        /// <summary>
        /// Queries this repository - provides ability to query using LINQ through entities inside repository.
        /// </summary>
        public override IQueryable<T> Query()
        {
            return base.Query().Where(p => !p.DeletedAtDateTimeUtc.HasValue);
        }
    }
}
