﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Extensions;

namespace EventsSystem.Data.Infrastructure.Impl
{
    /// <summary>
    /// The base repository
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class BaseRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{T}"/> class.
        /// </summary>
        /// <param name="contextFactory">The context factory.</param>
        protected BaseRepository(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <summary>
        /// Gets the all entities.
        /// </summary>
        /// <returns>Entities from repository</returns>
        public IEnumerable<T> GetAll()
        {
            return Query().ToList();
        }

        /// <summary>
        /// Asynchronously gets the all entities.
        /// </summary>
        /// <returns>Entities from repository</returns>
        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Query().ToListAsync();
        }

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Found entity or null</returns>
        public virtual T GetById(object id)
        {
            return ContextFactory.GetContext().GetById<T>(id);
        }

        /// <summary>
        /// Asynchronously gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity from repository</returns>
        public virtual async Task<T> GetByIdAsync(object id)
        {
            return await ContextFactory.GetContext().GetByIdAsync<T>(id);
        }

        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Add(T entity)
        {
            ContextFactory.GetContext().InsertAndSetIdentity<T>(entity);
            return this;
        }

        /// <summary>
        /// Asynchronously adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual async Task<IRepository<T>> AddAsync(T entity)
        {
            await ContextFactory.GetContext().InsertAndSetIdentityAsync<T>(entity);
            return this;
        }

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Update(T entity)
        {
            ContextFactory.GetContext().Update<T>(entity);
            return this;
        }

        /// <summary>
        /// Asynchronously updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual async Task<IRepository<T>> UpdateAsync(T entity)
        {
            await ContextFactory.GetContext().UpdateAsync<T>(entity);
            return this;
        }

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual IRepository<T> Delete(T entity)
        {
            ContextFactory.GetContext().Delete<T>(entity);
            return this;
        }

        /// <summary>
        /// Asynchronously deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        public virtual async Task<IRepository<T>> DeleteAsync(T entity)
        {
            await ContextFactory.GetContext().DeleteAsync<T>(entity);
            return this;
        }

        /// <summary>
        /// Queries this repository - provides ability to query using LINQ through entities inside repository.
        /// </summary>
        public virtual IQueryable<T> Query()
        {
            return ContextFactory.GetContext().GetTable<T>();
        }

        protected IContextFactory ContextFactory
        {
            get { return _contextFactory; }
        }

        /// <summary>
        /// Current instance of the context factory
        /// </summary>
        private readonly IContextFactory _contextFactory;
    }
}
