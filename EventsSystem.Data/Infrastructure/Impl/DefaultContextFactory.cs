﻿using System;

namespace EventsSystem.Data.Infrastructure.Impl
{
    /// <summary>
    /// The default context factory implementation
    /// </summary>
    public class DefaultContextFactory : Disposable, IContextFactory
    {
        /// <summary>
        /// Performs actual disposing
        /// </summary>
        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }

        /// <summary>
        /// Gets the current context from the factory.
        /// </summary>
        /// <returns></returns>
        public ApplicationDataConnection GetContext()
        {
            if (IsDisposed)
                throw new ObjectDisposedException("ContextFactory");

            return _dataContext ?? (_dataContext = new ApplicationDataConnection());
        }

        /// <summary>
        /// The cached instacne of the data context
        /// </summary>
        private ApplicationDataConnection _dataContext;
    }
}
