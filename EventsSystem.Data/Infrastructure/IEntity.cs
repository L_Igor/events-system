﻿namespace EventsSystem.Data.Infrastructure
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}