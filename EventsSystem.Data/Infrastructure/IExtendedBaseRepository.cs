﻿using System;
using System.Threading.Tasks;

namespace EventsSystem.Data.Infrastructure
{
    /// <summary>
    /// Provides basic interface for entity extended repositories
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    /// <typeparam name="TKey">Primary key type</typeparam>
    public interface IExtendedBaseRepository<T, TKey> : IRepository<T>
        where T : class, IEntity<TKey>, ITimestampEntity
        where TKey : IEquatable<TKey>
    {
        /// <summary>
        /// Deletes the specified entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IExtendedBaseRepository<T, TKey> Delete(TKey id, bool isSoftDelete = true);

        /// <summary>
        /// Asynchronously deletes the specified entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        Task<IExtendedBaseRepository<T, TKey>> DeleteAsync(TKey id, bool isSoftDelete = true);
    }
}