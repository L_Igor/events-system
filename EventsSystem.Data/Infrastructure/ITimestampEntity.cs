﻿using System;

namespace EventsSystem.Data.Infrastructure
{
    public interface ITimestampEntity
    {
        DateTime CreatedAtDateTimeUtc { get; set; }

        DateTime? UpdatedAtDateTimeUtc { get; set; }

        DateTime? DeletedAtDateTimeUtc { get; set; }
    }
}