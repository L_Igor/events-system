﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventsSystem.Data.Infrastructure
{
    /// <summary>
    /// Provides basic interface for entity repositories
    /// </summary>
    /// <typeparam name="T">Entity type</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets all entities.
        /// </summary>
        /// <returns>Entities from repository</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Asynchronously gets all entities.
        /// </summary>
        /// <returns>Entities from repository</returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity from repository</returns>
        T GetById(object id);

        /// <summary>
        /// Asynchronously gets the entity by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity from repository</returns>
        Task<T> GetByIdAsync(object id);

        /// <summary>
        /// Adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Add(T entity);

        /// <summary>
        /// Asynchronously adds the specified entity into the repository.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        Task<IRepository<T>> AddAsync(T entity);

        /// <summary>
        /// Updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Update(T entity);

        /// <summary>
        /// Asynchronously updates the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        Task<IRepository<T>> UpdateAsync(T entity);

        /// <summary>
        /// Deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        IRepository<T> Delete(T entity);

        /// <summary>
        /// Asynchronously deletes the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>
        /// Current repository instance for chained operations
        /// </returns>
        Task<IRepository<T>> DeleteAsync(T entity);

        /// <summary>
        /// Queries this repository - provides ability to query using LINQ through entities inside repository.
        /// </summary>
        /// <returns></returns>
        IQueryable<T> Query();
    }
}
