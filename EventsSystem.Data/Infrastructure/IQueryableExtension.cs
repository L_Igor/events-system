﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LinqToDB;

namespace EventsSystem.Data.Infrastructure
{
    public static class IQueryableExtension
    {
        public static IQueryable<T> LoadWith<T>(this IQueryable<T> source, Expression<Func<T, object>> path)
        {
            var table = source as ITable<T>;

            if (table != null)
            {
                return LinqExtensions.LoadWith(table, path);
            }

            return source;
        }

        public static async Task<List<T>> ToListAsync<T>(this IQueryable<T> source)
        {
            return await AsyncExtensions.ToListAsync(source);
        }

        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> source)
        {
            return await AsyncExtensions.FirstOrDefaultAsync(source);
        }

        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> source, Expression<Func<T, bool>> predicate)
        {
            return await AsyncExtensions.FirstOrDefaultAsync(source, predicate);
        }
    }
}
