﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventsSystem.Services.Dto.QA;

namespace EventsSystem.Services.Services
{
    /// <summary>
    /// Provides functionality for question and answer service. 
    /// </summary>
    public interface IQAService
    {
        /// <summary>
        /// Gets the all question and answers.
        /// </summary>
        /// <returns>The question and answers.</returns>
        Task<Result<IEnumerable<QADto>>> GetAllAsync();

        /// <summary>
        /// Gets the question and answer by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The question and answer.</returns>
        Task<Result<QADto>> GetByIdAsync(int id);

        /// <summary>
        /// Creates the question and answer.
        /// </summary>
        /// <param name="qa">The question and answer.</param>
        /// <returns>The identifier of the created question and answer.</returns>
        Task<Result<int>> CreateAsync(CreateQADto qa);

        /// <summary>
        /// Updates the question and answer.
        /// </summary>
        /// <param name="qa">The question and answer.</param>
        Task<Result> UpdateAsync(UpdateQADto qa);

        /// <summary>
        /// Deletes the question and answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(int id, bool isSoftDelete = true);

        /// <summary>
        /// Gets the all lecture questions.
        /// </summary>
        /// <param name="lectureId">The lecture identifier.</param>
        /// <returns>The lecture questions.</returns>
        Task<Result<IEnumerable<QADto>>> GetLectureQuestionsAsync(int lectureId);

        /// <summary>
        /// Creates the question.
        /// </summary>
        /// <param name="question">The question.</param>
        /// <returns>The identifier of the created question and answer.</returns>
        Task<Result<int>> CreateQuestionAsync(CreateQuestionDto question);

        /// <summary>
        /// Updates the question.
        /// </summary>
        /// <param name="question">The question.</param>
        Task<Result> UpdateQuestionAsync(UpdateQuestionDto question);

        /// <summary>
        /// Creates the answer.
        /// </summary>
        /// <param name="answer">The answer.</param>
        /// <returns>The identifier of the created question and answer.</returns>
        Task<Result<int>> CreateAnswerAsync(CreateAnswerDto answer);

        /// <summary>
        /// Updates the answer.
        /// </summary>
        /// <param name="answer">The answer.</param>
        Task<Result> UpdateAnswerAsync(UpdateAnswerDto answer);
    }
}