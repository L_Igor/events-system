﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;

namespace EventsSystem.Services.Services
{
    /// <summary>
    /// Provides functionality for lecture service. 
    /// </summary>
    public interface ILectureService
    {
        /// <summary>
        /// Gets the all lectures.
        /// </summary>
        /// <returns>The lectures.</returns>
        Task<Result<IEnumerable<LectureDto>>> GetAllAsync();

        /// <summary>
        /// Gets the all user lectures.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns>The user lectures.</returns>
        Task<Result<IEnumerable<LectureDto>>> GetUserLecturesAsync(int userId);

        /// <summary>
        /// Gets the lecture by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The lecture.</returns>
        Task<Result<LectureDto>> GetByIdAsync(int id);

        /// <summary>
        /// Creates the lecture.
        /// </summary>
        /// <param name="lecture">The lecture.</param>
        /// <returns>The identifier of the created lecture.</returns>
        Task<Result<int>> CreateAsync(CreateLectureDto lecture);

        /// <summary>
        /// Updates the lecture.
        /// </summary>
        /// <param name="lecture">The lecture.</param>
        Task<Result> UpdateAsync(UpdateLectureDto lecture);

        /// <summary>
        /// Deletes the lecture.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(int id, bool isSoftDelete = true);

        /// <summary>
        /// Deletes the lectures.
        /// </summary>
        /// <param name="id">The list of identifiers.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(IEnumerable<int> ids, bool isSoftDelete = true);

        /// <summary>
        /// Adds a lecture rating.
        /// </summary>
        /// <param name="lectureRating">The lecture rating.</param>
        Task<Result> AddRatingAsync(CreateLectureRatingDto lectureRating);
    }
}