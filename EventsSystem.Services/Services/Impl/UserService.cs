﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Core.Helpers;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Repository;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Services.Services.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IUserService"/>.
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserService"/> class.
        /// </summary>
        /// <param name="userRepository">The user repository.</param>
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Result<IEnumerable<UserDto>>> GetAllAsync()
        {
            var result = new Result<IEnumerable<UserDto>>();

            var users = await _userRepository.GetAllAsync();

            result.Data = Mapper.Map<IEnumerable<UserDto>>(users);

            return result;
        }

        public async Task<Result<UserDto>> GetByIdAsync(int id)
        {
            var result = new Result<UserDto>();

            var user = await _userRepository.GetByIdAsync(id);

            result.Data = Mapper.Map<UserDto>(user);

            return result;
        }

        public async Task<Result<UserDto>> GetByEmailAsync(string email)
        {
            var result = new Result<UserDto>();

            var user = await _userRepository.GetByEmailAsync(email);

            result.Data = Mapper.Map<UserDto>(user);

            return result;
        }

        public async Task<Result<UserDto>> GetByNameAsync(string userName)
        {
            var result = new Result<UserDto>();

            var user = await _userRepository.GetByNameAsync(userName);

            result.Data = Mapper.Map<UserDto>(user);

            return result;
        }

        public async Task<Result<int>> CreateAsync(CreateUserDto user)
        {
            var result = new Result<int>();

            var userDomain = Mapper.Map<User>(user);

            return await CreateAsync(userDomain);
        }

        public async Task<Result<int>> CreateAsync(CreateUserDto user, string password)
        {
            var result = new Result<int>();

            var passwordHash = AuthHelper.GetPassword(password, out var salt);
            var userDomain = Mapper.Map<User>(user);

            userDomain.PasswordHash = passwordHash;
            userDomain.Salt = salt;

            return await CreateAsync(userDomain);
        }

        public async Task<Result> SavePhoto(SaveUserPhotoDto photo)
        {
            var result = new Result();

            var userDomain = await _userRepository.GetByIdAsync(photo.Id);
            if (userDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<SaveUserPhotoDto, User>(photo, userDomain);
            await _userRepository.UpdateAsync(userDomain);

            return result;
        }

        public async Task<Result> UpdateAsync(UpdateUserDto user)
        {
            var result = new Result();

            var userDomain = await _userRepository.GetByIdAsync(user.Id);
            if (userDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateUserDto, User>(user, userDomain);
            await _userRepository.UpdateAsync(userDomain);

            return result;
        }

        public async Task<Result> DeleteAsync(int id, bool isSoftDelete = true)
        {
            var result = new Result();

            var userDomain = await _userRepository.GetByIdAsync(id);
            if (userDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            await _userRepository.DeleteAsync(id, isSoftDelete);

            return result;
        }

        public async Task<Result> DeleteAsync(IEnumerable<int> ids, bool isSoftDelete = true)
        {
            var result = new Result();

            foreach (var id in ids)
            {
                await _userRepository.DeleteAsync(id, isSoftDelete);
            }

            return result;
        }

        private async Task<Result<int>> CreateAsync(User user)
        {
            var result = new Result<int>();

            user.UserName = user.Email;

            var existingUser = await _userRepository.GetByNameAsync(user.UserName);
            if (existingUser != null)
            {
                result.AddError(ErrorType.NotUnique);
                return result;
            }

            await _userRepository.AddAsync(user);

            result.Data = user.Id;

            return result;
        }
    }
}
