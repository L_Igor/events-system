﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;
using EventsSystem.Data.Repository;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;

namespace EventsSystem.Services.Services.Impl
{
    /// <summary>
    /// Default implementation of <see cref="ILectureService"/>.
    /// </summary>
    public class LectureService : ILectureService
    {
        private readonly ILectureRepository _lectureRepository;
        private readonly ILectureRatingRepository _lectureRatingRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="LectureService"/> class.
        /// </summary>
        /// <param name="lectureRepository">The lecture repository.</param>
        /// <param name="lectureRatingRepository">The lecture rating repository.</param>
        public LectureService(ILectureRepository lectureRepository, ILectureRatingRepository lectureRatingRepository)
        {
            _lectureRepository = lectureRepository;
            _lectureRatingRepository = lectureRatingRepository;
        }

        public async Task<Result<IEnumerable<LectureDto>>> GetAllAsync()
        {
            var result = new Result<IEnumerable<LectureDto>>();

            var lectures = await _lectureRepository.GetAllAsync();

            result.Data = Mapper.Map<IEnumerable<LectureDto>>(lectures);

            return result;
        }

        public async Task<Result<IEnumerable<LectureDto>>> GetUserLecturesAsync(int userId)
        {
            var result = new Result<IEnumerable<LectureDto>>();

            var lectures =
                _lectureRepository.Query()
                    .Where(p => p.UserId.Equals(userId));

            result.Data = Mapper.Map<IEnumerable<LectureDto>>(await lectures.ToListAsync());

            return result;
        }

        public async Task<Result<LectureDto>> GetByIdAsync(int id)
        {
            var result = new Result<LectureDto>();

            var lecture = await _lectureRepository.GetByIdAsync(id);

            result.Data = Mapper.Map<LectureDto>(lecture);

            return result;
        }

        public async Task<Result<int>> CreateAsync(CreateLectureDto lecture)
        {
            var result = new Result<int>();

            var lectureDomain = Mapper.Map<Lecture>(lecture);
            await _lectureRepository.AddAsync(lectureDomain);

            result.Data = lectureDomain.Id;

            return result;
        }

        public async Task<Result> UpdateAsync(UpdateLectureDto lecture)
        {
            var result = new Result();

            var lectureDomain = await _lectureRepository.GetByIdAsync(lecture.Id);
            if (lectureDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateLectureDto, Lecture>(lecture, lectureDomain);
            await _lectureRepository.UpdateAsync(lectureDomain);

            return result;
        }

        public async Task<Result> DeleteAsync(int id, bool isSoftDelete = true)
        {
            var result = new Result();

            var lectureDomain = await _lectureRepository.GetByIdAsync(id);
            if (lectureDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            await _lectureRepository.DeleteAsync(id, isSoftDelete);

            return result;
        }

        public async Task<Result> DeleteAsync(IEnumerable<int> ids, bool isSoftDelete = true)
        {
            var result = new Result();

            foreach (var id in ids)
            {
                await _lectureRepository.DeleteAsync(id, isSoftDelete);
            }

            return result;
        }

        public async Task<Result> AddRatingAsync(CreateLectureRatingDto lectureRating)
        {
            var result = new Result();

            var lectureRatingDomain = Mapper.Map<LectureRating>(lectureRating);
            _lectureRatingRepository.Add(lectureRatingDomain);

            var lectureDomain = await _lectureRepository.GetByIdAsync(lectureRating.LectureId);
            lectureDomain.SumRating += lectureRating.Rating;
            lectureDomain.CountRatings++;
            lectureDomain.Rating = (float)lectureDomain.SumRating / lectureDomain.CountRatings;
            await _lectureRepository.UpdateAsync(lectureDomain);

            return result;
        }
    }
}
