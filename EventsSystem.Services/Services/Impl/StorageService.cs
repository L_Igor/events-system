﻿using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Repository;
using EventsSystem.Services.Dto.Storage;

namespace EventsSystem.Services.Services.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IStorageService"/>.
    /// </summary>
    public class StorageService : IStorageService
    {
        private readonly IStorageRepository _storageRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageService"/> class.
        /// </summary>
        /// <param name="storageRepository">The storage repository.</param>
        public StorageService(IStorageRepository storageRepository)
        {
            _storageRepository = storageRepository;
        }

        public async Task<Result<StorageDto>> GetByIdAsync(int id)
        {
            var result = new Result<StorageDto>();

            var storageModel = await _storageRepository.GetByIdAsync(id);

            result.Data = Mapper.Map<StorageDto>(storageModel);

            return result;
        }

        public async Task<Result<int>> CreateAsync(CreateStorageDto storageModel)
        {
            var result = new Result<int>();

            var storageDomain = Mapper.Map<Storage>(storageModel);
            await _storageRepository.AddAsync(storageDomain);

            result.Data = storageDomain.Id;

            return result;
        }

        public async Task<Result> DeleteAsync(int id, bool isSoftDelete = true)
        {
            var result = new Result();

            var storageDomain = await _storageRepository.GetByIdAsync(id);
            if (storageDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            await _storageRepository.DeleteAsync(id, isSoftDelete);

            return result;
        }
    }
}
