﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Infrastructure;
using EventsSystem.Data.Repository;
using EventsSystem.Services.Dto.QA;

namespace EventsSystem.Services.Services.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IQAService"/>.
    /// </summary>
    public class QAService : IQAService
    {
        private readonly IQARepository _qaRepository;
        private readonly ILectureRepository _lectureRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="QAService"/> class.
        /// </summary>
        /// <param name="qaRepository">The question and answer repository.</param>
        /// <param name="lectureRepository">The lecture repository.</param>
        public QAService(IQARepository qaRepository, ILectureRepository lectureRepository)
        {
            _qaRepository = qaRepository;
            _lectureRepository = lectureRepository;
        }

        public async Task<Result<IEnumerable<QADto>>> GetAllAsync()
        {
            var result = new Result<IEnumerable<QADto>>();

            var qas = await _qaRepository.GetAllAsync();

            result.Data = Mapper.Map<IEnumerable<QADto>>(qas);

            return result;
        }

        public async Task<Result<QADto>> GetByIdAsync(int id)
        {
            var result = new Result<QADto>();

            var qa = await _qaRepository.GetByIdAsync(id);

            result.Data = Mapper.Map<QADto>(qa);

            return result;
        }

        public async Task<Result<int>> CreateAsync(CreateQADto qa)
        {
            var result = new Result<int>();

            var qaDomain = new QA()
            {
                QuestionDateTime = DateTimeOffset.UtcNow,
                AnswerDateTime = DateTimeOffset.UtcNow,
            };
            Mapper.Map<CreateQADto, QA>(qa, qaDomain);
            await _qaRepository.AddAsync(qaDomain);
            await IncrementCountQuestins(qaDomain.LectureId);

            result.Data = qaDomain.Id;

            return result;
        }

        public async Task<Result> UpdateAsync(UpdateQADto qa)
        {
            var result = new Result();

            var qaDomain = await _qaRepository.GetByIdAsync(qa.Id);
            if (qaDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateQADto, QA>(qa, qaDomain);
            await _qaRepository.UpdateAsync(qaDomain);

            return result;
        }

        public async Task<Result> DeleteAsync(int id, bool isSoftDelete = true)
        {
            var result = new Result();

            var qaDomain = await _qaRepository.GetByIdAsync(id);
            if (qaDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            await _qaRepository.DeleteAsync(id, isSoftDelete);
            await DecrementCountQuestins(qaDomain.LectureId);

            return result;
        }

        public async Task<Result<IEnumerable<QADto>>> GetLectureQuestionsAsync(int lectureId)
        {
            var result = new Result<IEnumerable<QADto>>();

            var qa =
                _qaRepository.Query()
                    .Where(p => p.LectureId.Equals(lectureId));

            result.Data = Mapper.Map<IEnumerable<QADto>>(await qa.ToListAsync());

            return result;
        }

        public async Task<Result<int>> CreateQuestionAsync(CreateQuestionDto question)
        {
            var result = new Result<int>();

            var qaDomain = new QA()
            {
                QuestionDateTime = DateTimeOffset.UtcNow,
            };
            Mapper.Map<CreateQuestionDto, QA>(question, qaDomain);
            await _qaRepository.AddAsync(qaDomain);
            await IncrementCountQuestins(qaDomain.LectureId);

            result.Data = qaDomain.Id;

            return result;
        }

        public async Task<Result> UpdateQuestionAsync(UpdateQuestionDto question)
        {
            var result = new Result();

            var qaDomain = await _qaRepository.GetByIdAsync(question.Id);
            if (qaDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateQuestionDto, QA>(question, qaDomain);
            _qaRepository.Update(qaDomain);

            return result;
        }

        public async Task<Result<int>> CreateAnswerAsync(CreateAnswerDto answer)
        {
            var result = new Result<int>();

            var qaDomain = await _qaRepository.GetByIdAsync(answer.QuestionId);
            if (qaDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            qaDomain.AnswerDateTime = DateTimeOffset.UtcNow;
            Mapper.Map<CreateAnswerDto, QA>(answer, qaDomain);
            await _qaRepository.UpdateAsync(qaDomain);

            result.Data = qaDomain.Id;

            return result;
        }

        public async Task<Result> UpdateAnswerAsync(UpdateAnswerDto answer)
        {
            var result = new Result();

            var qaDomain = await _qaRepository.GetByIdAsync(answer.Id);
            if (qaDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateAnswerDto, QA>(answer, qaDomain);
            await _qaRepository.UpdateAsync(qaDomain);

            return result;
        }

        private async Task IncrementCountQuestins(int lectureId)
        {
            await UpdateCountQuestions(lectureId, l => l.CountQuestions++);
        }

        private async Task DecrementCountQuestins(int lectureId)
        {
            await UpdateCountQuestions(lectureId, l => l.CountQuestions--);
        }

        private async Task UpdateCountQuestions(int lectureId, Action<Lecture> action)
        {
            var lecture = await _lectureRepository.GetByIdAsync(lectureId);
            if (lecture == null)
            {
                return;
            }
            action.Invoke(lecture);
            await _lectureRepository.UpdateAsync(lecture);
        }
    }
}
