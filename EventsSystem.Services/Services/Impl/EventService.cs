﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Repository;
using EventsSystem.Services.Dto.Event;

namespace EventsSystem.Services.Services.Impl
{
    /// <summary>
    /// Default implementation of <see cref="IEventService"/>.
    /// </summary>
    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventService"/> class.
        /// </summary>
        /// <param name="eventRepository">The event repository.</param>
        public EventService(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<Result<IEnumerable<EventDto>>> GetAllAsync()
        {
            var result = new Result<IEnumerable<EventDto>>();

            var events = await _eventRepository.GetAllAsync();

            result.Data = Mapper.Map<IEnumerable<EventDto>>(events);

            return result;
        }

        public async Task<Result<EventDto>> GetByIdAsync(int id)
        {
            var result = new Result<EventDto>();

            var eventModel = await _eventRepository.GetByIdAsync(id);

            result.Data = Mapper.Map<EventDto>(eventModel);

            return result;
        }

        public async Task<Result<int>> CreateAsync(CreateEventDto eventModel)
        {
            var result = new Result<int>();

            var eventDomain = Mapper.Map<Event>(eventModel);
            await _eventRepository.AddAsync(eventDomain);

            result.Data = eventDomain.Id;

            return result;
        }

        public async Task<Result> UpdateAsync(UpdateEventDto eventModel)
        {
            var result = new Result();

            var eventDomain = await _eventRepository.GetByIdAsync(eventModel.Id);
            if (eventDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            Mapper.Map<UpdateEventDto, Event>(eventModel, eventDomain);
            await _eventRepository.UpdateAsync(eventDomain);

            return result;
        }

        public async Task<Result> DeleteAsync(int id, bool isSoftDelete = true)
        {
            var result = new Result();

            var eventDomain = await _eventRepository.GetByIdAsync(id);
            if (eventDomain == null)
            {
                result.AddError(ErrorType.NotFound);
                return result;
            }

            await _eventRepository.DeleteAsync(id, isSoftDelete);

            return result;
        }
    }
}
