﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventsSystem.Services.Dto.Event;

namespace EventsSystem.Services.Services
{
    /// <summary>
    /// Provides functionality for event service. 
    /// </summary>
    public interface IEventService
    {
        /// <summary>
        /// Gets the all events.
        /// </summary>
        /// <returns>The events.</returns>
        Task<Result<IEnumerable<EventDto>>> GetAllAsync();

        /// <summary>
        /// Gets the event by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The event.</returns>
        Task<Result<EventDto>> GetByIdAsync(int id);

        /// <summary>
        /// Creates the event.
        /// </summary>
        /// <param name="eventModel">The event.</param>
        /// <returns>The identifier of the created event.</returns>
        Task<Result<int>> CreateAsync(CreateEventDto eventModel);

        /// <summary>
        /// Updates the event.
        /// </summary>
        /// <param name="eventModel">The event.</param>
        Task<Result> UpdateAsync(UpdateEventDto eventModel);

        /// <summary>
        /// Deletes the event.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(int id, bool isSoftDelete = true);
    }
}