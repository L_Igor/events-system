﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Services.Services
{
    /// <summary>
    /// Provides functionality for user service. 
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Gets the all users.
        /// </summary>
        /// <returns>The users.</returns>
        Task<Result<IEnumerable<UserDto>>> GetAllAsync();

        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The user.</returns>
        Task<Result<UserDto>> GetByIdAsync(int id);

        /// <summary>
        /// Gets the user by email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns>The user.</returns>
        Task<Result<UserDto>> GetByEmailAsync(string email);

        /// <summary>
        /// Gets the user by username.
        /// </summary>
        /// <param name="userName">The username.</param>
        /// <returns>The user.</returns>
        Task<Result<UserDto>> GetByNameAsync(string userName);

        /// <summary>
        /// Creates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns>The identifier of the created user.</returns>
        Task<Result<int>> CreateAsync(CreateUserDto user);

        /// <summary>
        /// Creates the user with password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The user password.</param>
        /// <returns>The created user.</returns>
        Task<Result<int>> CreateAsync(CreateUserDto user, string password);

        /// <summary>
        /// Saves the user photo.
        /// </summary>
        /// <param name="photo">The user photo.</param>
        Task<Result> SavePhoto(SaveUserPhotoDto photo);

        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="user">The user.</param>
        Task<Result> UpdateAsync(UpdateUserDto user);

        /// <summary>
        /// Deletes the user.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(int id, bool isSoftDelete = true);

        /// <summary>
        /// Deletes the users.
        /// </summary>
        /// <param name="id">The list of identifiers.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(IEnumerable<int> ids, bool isSoftDelete = true);
    }
}