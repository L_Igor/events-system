﻿using System.Threading.Tasks;
using EventsSystem.Services.Dto.Storage;

namespace EventsSystem.Services.Services
{
    /// <summary>
    /// Provides functionality for storage service. 
    /// </summary>
    public interface IStorageService
    {
        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The storage item.</returns>
        Task<Result<StorageDto>> GetByIdAsync(int id);

        /// <summary>
        /// Creates the storage item.
        /// </summary>
        /// <param name="storageItem">The storage item.</param>
        /// <returns>The identifier of the created storage item.</returns>
        Task<Result<int>> CreateAsync(CreateStorageDto storageItem);

        /// <summary>
        /// Deletes the storage item.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="isSoftDelete">If set to true, a soft delete is performed, otherwise the entity is physically deleted.</param>
        Task<Result> DeleteAsync(int id, bool isSoftDelete = true);
    }
}