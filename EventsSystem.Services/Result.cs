﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventsSystem.Services
{
    public class Result
    {
        private readonly Dictionary<string, Error> _errors;

        public Result()
        {
            _errors = new Dictionary<string, Error>();
        }

        public bool HasErrors
        {
            get { return Errors.Any(); }
        }

        public IEnumerable<Error> Errors
        {
            get { return _errors.Values; }
        }

        public void AddError(ErrorType errorType, string fieldName = "", string message = "")
        {

            if (!_errors.ContainsKey(fieldName))
            {
                _errors.Add(fieldName, new Error()
                {
                    ErrorType = errorType,
                    FieldName = fieldName,
                    ErrorMessages = new List<ErrorMessage>(),
                });
            }

            ((List<ErrorMessage>)_errors[fieldName].ErrorMessages).Add(new ErrorMessage() { Message = message });
        }

        public bool ContainsError(ErrorType errorType)
        {
            return Errors.Any(p => p.ErrorType == errorType);
        }
    }

    public class Result<T> : Result
    {
        public T Data { get; set; }
    }

    public class Error
    {
        public ErrorType ErrorType { get; set; }

        public string FieldName { get; set; }

        public IEnumerable<ErrorMessage> ErrorMessages { get; set; }
    }

    public class ErrorMessage
    {
        public string Message { get; set; }
    }

    public enum ErrorType
    {
        NotFound,
        NotUnique,
    }
}
