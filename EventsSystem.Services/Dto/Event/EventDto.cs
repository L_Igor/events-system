﻿using System;

namespace EventsSystem.Services.Dto.Event
{
    /// <summary>
    /// Represents an event.
    /// </summary>
    public class EventDto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the event was started.
        /// </summary>
        public DateTimeOffset StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the event was ended.
        /// </summary>
        public DateTimeOffset EndDateTime { get; set; }
    }
}
