﻿using System;

namespace EventsSystem.Services.Dto.Lecture
{
    /// <summary>
    /// Represents a lecture.
    /// </summary>
    public class LectureDto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the lecture was started.
        /// </summary>
        public DateTimeOffset StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the lecture was ended.
        /// </summary>
        public DateTimeOffset EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public float Rating { get; set; }

        /// <summary>
        /// Gets or sets the number of ratings.
        /// </summary>
        public int CountRatings { get; set; }

        /// <summary>
        /// Gets or sets the number of questions.
        /// </summary>
        public int CountQuestions { get; set; }

        /// <summary>
        /// Gets or sets the event identifier.
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the speaker identifier.
        /// </summary>
        public int UserId { get; set; }
    }
}
