﻿namespace EventsSystem.Services.Dto.User
{
    /// <summary>
    /// Represents a photo.
    /// </summary>
    public class SaveUserPhotoDto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the photo path.
        /// </summary>
        public string Photo { get; set; }
        
        /// <summary>
        /// Gets or sets the photo identifier.
        /// </summary>
        public int PhotoId { get; set; }
    }
}
