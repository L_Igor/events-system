﻿namespace EventsSystem.Services.Dto.User
{
    /// <summary>
    /// Represents a user.
    /// </summary>
    public class CreateUserDto
    {
        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the bio.
        /// </summary>
        public string Bio { get; set; }

        /// <summary>
        /// Gets or sets the occupation.
        /// </summary>
        public string Occupation { get; set; }

        /// <summary>
        /// Gets or sets the photo path.
        /// </summary>
        public string Photo { get; set; }
    }
}
