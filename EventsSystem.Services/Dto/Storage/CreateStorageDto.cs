﻿namespace EventsSystem.Services.Dto.Storage
{
    /// <summary>
    /// Represents a storage.
    /// </summary>
    public class CreateStorageDto
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the mime type.
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the length.
        /// </summary>
        public long Length { get; set; }

        /// <summary>
        /// Gets or sets the original file name.
        /// </summary>
        public string OriginalFileName { get; set; }
    }
}
