﻿namespace EventsSystem.Services.Dto.QA
{
    /// <summary>
    /// Represents a question and answer.
    /// </summary>
    public class UpdateQADto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Gets or sets the user asking identifier.
        /// </summary>
        public int AskingId { get; set; }

        /// <summary>
        /// Gets or sets the user answering identifier.
        /// </summary>
        public int? AnsweringId { get; set; }

        /// <summary>
        /// Gets or sets the lecture identifier.
        /// </summary>
        public int LectureId { get; set; }
    }
}
