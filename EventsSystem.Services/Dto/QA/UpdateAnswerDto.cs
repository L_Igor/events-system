﻿namespace EventsSystem.Services.Dto.QA
{
    /// <summary>
    /// Represents an answer.
    /// </summary>
    public class UpdateAnswerDto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        public string Answer { get; set; }
    }
}
