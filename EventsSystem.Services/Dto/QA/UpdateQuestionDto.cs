﻿namespace EventsSystem.Services.Dto.QA
{
    /// <summary>
    /// Represents a question.
    /// </summary>
    public class UpdateQuestionDto
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }
    }
}
