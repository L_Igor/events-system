﻿namespace EventsSystem.Services.Dto.QA
{
    /// <summary>
    /// Represents a question.
    /// </summary>
    public class CreateQuestionDto
    {
        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the user asking identifier.
        /// </summary>
        public int AskingId { get; set; }

        /// <summary>
        /// Gets or sets the lecture identifier.
        /// </summary>
        public int LectureId { get; set; }
    }
}
