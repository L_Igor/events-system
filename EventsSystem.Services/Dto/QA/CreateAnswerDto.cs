﻿namespace EventsSystem.Services.Dto.QA
{
    /// <summary>
    /// Represents an answer.
    /// </summary>
    public class CreateAnswerDto
    {
        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Gets or sets the user answering identifier.
        /// </summary>
        public int AnsweringId { get; set; }

        /// <summary>
        /// Gets or sets the question identifier.
        /// </summary>
        public int QuestionId { get; set; }
    }
}
