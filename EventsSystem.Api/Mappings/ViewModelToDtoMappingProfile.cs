﻿using AutoMapper;
using EventsSystem.Api.V0.Models.ViewModels.Account;
using EventsSystem.Api.V0.Models.ViewModels.Event;
using EventsSystem.Api.V0.Models.ViewModels.Lecture;
using EventsSystem.Api.V0.Models.ViewModels.LectureRating;
using EventsSystem.Api.V0.Models.ViewModels.QA;
using EventsSystem.Api.V0.Models.ViewModels.User;
using EventsSystem.Services.Dto.Event;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Api.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert View Model classes to DTO Model classes.
    /// </summary>
    public class ViewModelToDtoMappingProfile : Profile
    {
        public ViewModelToDtoMappingProfile()
        {
            CreateMap<CreateEventVm, CreateEventDto>();
            CreateMap<UpdateEventVm, UpdateEventDto>();

            CreateMap<CreateLectureVm, CreateLectureDto>();
            CreateMap<UpdateLectureVm, UpdateLectureDto>();

            CreateMap<CreateLectureRatingVm, CreateLectureRatingDto>();
            CreateMap<UpdateLectureRatingVm, UpdateLectureRatingDto>();

            CreateMap<CreateQAVm, CreateQADto>();
            CreateMap<UpdateQAVm, UpdateQADto>();

            CreateMap<CreateQuestionVm, CreateQuestionDto>();
            CreateMap<UpdateQuestionVm, UpdateQuestionDto>();

            CreateMap<CreateAnswerVm, CreateAnswerDto>();
            CreateMap<UpdateAnswerVm, UpdateAnswerDto>();

            CreateMap<CreateUserVm, CreateUserDto>();
            CreateMap<UpdateUserVm, UpdateUserDto>();

            CreateMap<RegisterVm, CreateUserDto>();

            CreateMap<SaveUserPhotoVm, SaveUserPhotoDto>();
        }
    }
}