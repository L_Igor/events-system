﻿using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Services.Dto.Event;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Dto.Storage;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Api.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert Domain Model entities to DTO Model classes.
    /// </summary>
    public class DomainModelToDtoMappingProfile : Profile
    {
        public DomainModelToDtoMappingProfile()
        {
            CreateMap<Event, EventDto>();

            CreateMap<Lecture, LectureDto>();

            CreateMap<LectureRating, LectureRatingDto>();

            CreateMap<QA, QADto>();

            CreateMap<User, UserDto>();

            CreateMap<Storage, StorageDto>();
        }
    }
}