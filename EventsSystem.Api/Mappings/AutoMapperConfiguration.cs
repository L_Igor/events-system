﻿using AutoMapper;

namespace EventsSystem.Api.Mappings
{
    /// <summary>
    /// Contains configuration routines for AutoMapper
    /// </summary>
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// Configures AutoMapper. Should be called on application start.
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ViewModelToDtoMappingProfile>();
                x.AddProfile<DtoToDomainModelMappingProfile>();
                x.AddProfile<DomainModelToDtoMappingProfile>();
                x.AddProfile<DtoToViewModelMappingProfile>();
            });
        }
    }
}