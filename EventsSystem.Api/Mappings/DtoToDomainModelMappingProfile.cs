﻿using AutoMapper;
using EventsSystem.Data.Entities;
using EventsSystem.Services.Dto.Event;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Dto.Storage;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Api.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert DTO Model classes to Domain Model entities.
    /// </summary>
    public class DtoToDomainModelMappingProfile : Profile
    {
        public DtoToDomainModelMappingProfile()
        {
            CreateMap<CreateEventDto, Event>();
            CreateMap<UpdateEventDto, Event>();

            CreateMap<CreateLectureDto, Lecture>();
            CreateMap<UpdateLectureDto, Lecture>();

            CreateMap<CreateLectureRatingDto, LectureRating>();
            CreateMap<UpdateLectureRatingDto, LectureRating>();

            CreateMap<CreateQADto, QA>();
            CreateMap<UpdateQADto, QA>();
            CreateMap<CreateQuestionDto, QA>();
            CreateMap<UpdateQuestionDto, QA>();
            CreateMap<CreateAnswerDto, QA>();
            CreateMap<UpdateAnswerDto, QA>();

            CreateMap<CreateUserDto, User>();
            CreateMap<UpdateUserDto, User>();
            CreateMap<SaveUserPhotoDto, User>();

            CreateMap<CreateStorageDto, Storage>();
        }
    }
}