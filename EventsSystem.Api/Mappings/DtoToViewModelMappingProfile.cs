﻿using AutoMapper;
using EventsSystem.Api.V0.Models.ViewModels.Event;
using EventsSystem.Api.V0.Models.ViewModels.Lecture;
using EventsSystem.Api.V0.Models.ViewModels.LectureRating;
using EventsSystem.Api.V0.Models.ViewModels.QA;
using EventsSystem.Api.V0.Models.ViewModels.User;
using EventsSystem.Services.Dto.Event;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Dto.User;

namespace EventsSystem.Api.Mappings
{
    /// <summary>
    /// Contains mapping configuration for AutoMapper that should convert DTO Model classes to View Model classes.
    /// </summary>
    public class DtoToViewModelMappingProfile : Profile
    {
        public DtoToViewModelMappingProfile()
        {
            CreateMap<EventDto, EventVm>();

            CreateMap<LectureDto, LectureVm>();

            CreateMap<LectureRatingDto, LectureRatingVm>();

            CreateMap<QADto, QAVm>();

            CreateMap<UserDto, UserVm>();
        }
    }
}