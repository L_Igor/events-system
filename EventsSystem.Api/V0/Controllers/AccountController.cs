﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.Core.ActionFilters;
using EventsSystem.Api.V0.Models.ViewModels.Account;
using EventsSystem.Api.V0.Models.ViewModels.User;
using EventsSystem.Services.Dto.User;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents an account service.
    /// </summary>
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController" /> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="model">The user credentials.</param>
        /// <returns>The created user.</returns>
        /// <response code="200">The successfully retrieved user.</response>
        /// <response code="400">The user credentials is invalid.</response>
        /// <response code="409">The specified user exists.</response>
        [AllowAnonymous]
        [HttpPost("Register")]
        [ValidateModel]
        [ProducesResponseType(typeof(UserVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> Post([FromBody] RegisterVm model)
        {
            var createUserDto = Mapper.Map<CreateUserDto>(model);
            var result = await _userService.CreateAsync(createUserDto, model.Password);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var userDtoResult = await _userService.GetByIdAsync(result.Data);
            var userDto = userDtoResult.Data;
            var userViewModel = Mapper.Map<UserVm>(userDto);

            return Ok(userViewModel);
        }
    }
}
