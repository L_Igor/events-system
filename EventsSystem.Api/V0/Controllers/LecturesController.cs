﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.Core.ActionFilters;
using EventsSystem.Api.V0.Models;
using EventsSystem.Api.V0.Models.ViewModels.Lecture;
using EventsSystem.Api.V0.Models.ViewModels.LectureRating;
using EventsSystem.Api.V0.Models.ViewModels.QA;
using EventsSystem.Services.Dto.Lecture;
using EventsSystem.Services.Dto.LectureRating;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST lecture service.
    /// </summary>
    [Authorize]
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class LecturesController : BaseController
    {
        private readonly ILectureService _lectureService;

        private const string GetByIdRouteName = nameof(V0) + nameof(LecturesController) + "GetById";

        /// <summary>
        /// Initializes a new instance of the <see cref="LecturesController" /> class.
        /// </summary>
        /// <param name="lectureService">The lecture service.</param>
        public LecturesController(ILectureService lectureService)
        {
            _lectureService = lectureService;
        }

        /// <summary>
        /// Gets all lecture.
        /// </summary>
        /// <returns>All available lecture.</returns>
        /// <response code="200">The successfully retrieved lecture.</response>
        [AllowAnonymous]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<LectureVm>), 200)]
        public async Task<IActionResult> Get()
        {
            var result = await _lectureService.GetAllAsync();
            var lecturesDto = result.Data;
            var lecturesViewModel = Mapper.Map<IEnumerable<LectureVm>>(lecturesDto);

            return Ok(lecturesViewModel);
        }

        /// <summary>
        /// Gets a single lecture.
        /// </summary>
        /// <param name="id">The requested lecture identifier.</param>
        /// <returns>The requested lecture.</returns>
        /// <response code="200">The lecture was successfully retrieved.</response>
        /// <response code="404">The lecture does not exist.</response>
        [AllowAnonymous]
        [HttpGet("{id}", Name = GetByIdRouteName)]
        [ProducesResponseType(typeof(LectureVm), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _lectureService.GetByIdAsync(id);
            var lectureDto = result.Data;
            if (lectureDto == null)
            {
                return NotFound();
            }
            var lectureViewModel = Mapper.Map<LectureVm>(lectureDto);

            return Ok(lectureViewModel);
        }

        /// <summary>
        /// Creates a new lecture.
        /// </summary>
        /// <param name="model">The lecture to create.</param>
        /// <returns>The created lecture.</returns>
        /// <response code="201">The lecture was successfully created.</response>
        /// <response code="400">The lecture is invalid.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(typeof(LectureVm), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] CreateLectureVm model)
        {
            var createLectureDto = Mapper.Map<CreateLectureDto>(model);
            var result = await _lectureService.CreateAsync(createLectureDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var lectureDtoResult = await _lectureService.GetByIdAsync(result.Data);
            var lectureDto = lectureDtoResult.Data;
            var lectureViewModel = Mapper.Map<LectureVm>(lectureDto);

            return CreatedAtRoute(GetByIdRouteName, new { id = lectureViewModel.Id }, lectureViewModel);
        }

        /// <summary>
        /// Modifies an existing lecture.
        /// </summary>
        /// <param name="id">The modified lecture identifier.</param>
        /// <param name="model">The lecture to modify.</param>
        /// <returns>The modified lecture.</returns>
        /// <response code="200">The lecture was successfully modified.</response>
        /// <response code="400">The lecture is invalid.</response>
        /// <response code="404">The lecture does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(typeof(LectureVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateLectureVm model)
        {
            var updateLectureDto = new UpdateLectureDto()
            {
                Id = id,
            };
            Mapper.Map<UpdateLectureVm, UpdateLectureDto>(model, updateLectureDto);
            var result = await _lectureService.UpdateAsync(updateLectureDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var lectureDtoResult = await _lectureService.GetByIdAsync(id);
            var lectureDto = lectureDtoResult.Data;
            var lectureViewModel = Mapper.Map<LectureVm>(lectureDto);

            return Ok(lectureViewModel);
        }

        /// <summary>
        /// Deletes an existing lecture.
        /// </summary>
        /// <param name="id">The deleted lecture identifier.</param>
        /// <returns></returns>
        /// <response code="204">The lecture was successfully deleted.</response>
        /// <response code="404">The lecture does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _lectureService.DeleteAsync(id);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a list of existing lectures.
        /// </summary>
        /// <param name="ids">The list of identifiers.</param>
        /// <returns></returns>
        /// <response code="204">The lectures was successfully deleted.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpDelete]
        [ProducesResponseType(204)]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<int> ids)
        {
            var result = await _lectureService.DeleteAsync(ids);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a lecture rating.
        /// </summary>
        /// <param name="id">The lecture identifier.</param>
        /// <param name="model">The added rating.</param>
        /// <returns></returns>
        /// <response code="200">The rating was successfully added.</response>
        /// <response code="400">The rating is invalid.</response>
        /// <response code="404">The lecture does not exist.</response>
        [HttpPost("{id}/Ratings")]
        [ValidateModel]
        [ProducesResponseType(typeof(LectureVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PostRatings(int id, [FromBody] CreateLectureRatingVm model)
        {
            var createLectureRatingDto = new CreateLectureRatingDto()
            {
                LectureId = id,
                UserId = CurrentUser.Id,
            };
            Mapper.Map<CreateLectureRatingVm, CreateLectureRatingDto>(model, createLectureRatingDto);
            var result = await _lectureService.AddRatingAsync(createLectureRatingDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var lectureRatingDtoResult = await _lectureService.GetByIdAsync(id);
            var lectureRatingDto = lectureRatingDtoResult.Data;
            var lectureRatingViewModel = Mapper.Map<LectureVm>(lectureRatingDto);

            return Ok(lectureRatingViewModel);
        }

        /// <summary>
        /// Gets all questions of the specified lecture.
        /// </summary>
        /// <returns>All available lecture questions.</returns>
        /// <response code="200">The successfully retrieved lecture questions.</response>
        [AllowAnonymous]
        [HttpGet("{id}/Questions")]
        [ProducesResponseType(typeof(IEnumerable<QAVm>), 200)]
        public async Task<IActionResult> GetQuestions(int id)
        {
            var qaService = GetService<IQAService>();

            var result = await qaService.GetLectureQuestionsAsync(id);
            var qaDto = result.Data;
            var qaViewModel = Mapper.Map<IEnumerable<QAVm>>(qaDto);

            return Ok(qaViewModel);
        }

        /// <summary>
        /// Creates a new question.
        /// </summary>
        /// <param name="id">The lecture identifier.</param>
        /// <param name="model">The added question.</param>
        /// <returns>The created question and answer.</returns>
        /// <response code="200">The question was successfully created.</response>
        /// <response code="400">The question is invalid.</response>
        /// <response code="404">The lecture does not exist.</response>
        [HttpPost("{id}/Questions")]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PostQuestions(int id, [FromBody] CreateQuestionVm model)
        {
            var qaService = GetService<IQAService>();

            var createQuestionDto = new CreateQuestionDto()
            {
                LectureId = id,
                AskingId = CurrentUser.Id,
            };
            Mapper.Map<CreateQuestionVm, CreateQuestionDto>(model, createQuestionDto);
            var result = await qaService.CreateQuestionAsync(createQuestionDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var questionDtoResult = await qaService.GetByIdAsync(result.Data);
            var questionDto = questionDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(questionDto);

            return Ok(qaViewModel);
        }
    }
}