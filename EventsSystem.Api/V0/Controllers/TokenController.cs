﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using EventsSystem.Api.Settings;
using EventsSystem.Api.V0.Models.Auth;
using EventsSystem.Core.Helpers;
using EventsSystem.Data.Entities;
using EventsSystem.Data.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST token service.
    /// </summary>
	[ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class TokenController : BaseController
    {
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IUserRepository _userRepository;
        private readonly AuthSettings _authSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="TokenController" /> class.
        /// </summary>
        /// <param name="refreshTokenRepository">The refresh token repository.</param>
        /// <param name="userRepository">The user repository.</param>
        /// <param name="authSettings">The authentication configuration.</param>
        public TokenController(IRefreshTokenRepository refreshTokenRepository, IUserRepository userRepository,
            IOptions<AuthSettings> authSettings)
        {
            _refreshTokenRepository = refreshTokenRepository;
            _userRepository = userRepository;
            _authSettings = authSettings.Value;
        }

        /// <summary>
        /// Creates and updates an Access Token.
        /// </summary>
        /// <remarks>
        /// You can use the password grant to get an access token from a user logging in with their username and password.
        /// 
        ///     POST /Token
        ///     {
        ///         "grant_type": "password",
        ///         "username": "user@mail.ru",
        ///         "password": "Pf3g6Hkc"
        ///     }
        /// 
        /// If your access token expires, you can use a refresh token to get a new access token without having to re-authorize the user.
        /// 
        ///     POST /Token
        ///     {
        ///         "grant_type": "refresh_token",
        ///         "refresh_token": "5dd0d5264f344651acfd57bd245941ed"
        ///     }
        /// </remarks>
        /// <param name="model">The access token request.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(AccessTokenResponse), 200)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(typeof(ErrorResponse), 401)]
        public async Task<IActionResult> PostToken([FromBody] AccessTokenRequest model)
        {
            if (model.GrantType == GrantType.Password)
            {
                return await PostCreateToken(model as PasswordAccessTokenRequest);
            }
            else if (model.GrantType == GrantType.RefreshToken)
            {
                return await PostRefreshToken(model as RefreshAccessTokenRequest);
            }

            return BadRequest(new ErrorResponse
            {
                Error = ErrorCode.UnsupportedGrantType,
                ErrorDescription = $"Grant type '{model.GrantType}' is not supported.",
            });
        }

        /// <summary>
        /// Creates an Access Token.
        /// </summary>
        /// <param name="model">The <see cref="PasswordAccessTokenRequest"/> model.</param>
        /// <returns></returns>
        private async Task<IActionResult> PostCreateToken([FromBody] PasswordAccessTokenRequest model)
        {
            if (model.GrantType != GrantType.Password)
            {
                throw new ArgumentException($"Expected '{GrantType.Password}' value.", nameof(model.GrantType));
            }

            if (String.IsNullOrWhiteSpace(model.Username))
            {
                return BadRequest(new ErrorResponse
                {
                    Error = ErrorCode.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.Username)}' parameter.",
                });
            }

            if (String.IsNullOrWhiteSpace(model.Password))
            {
                return BadRequest(new ErrorResponse
                {
                    Error = ErrorCode.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.Password)}' parameter.",
                });
            }


            var credentials = await _userRepository.GetByNameAsync(model.Username);

            if (credentials == null)
            {
                return StatusCode(401, new ErrorResponse
                {
                    Error = ErrorCode.InvalidClient,
                    ErrorDescription = "Invalid username.",
                });
            }

            if (AuthHelper.GetPassword(model.Password, credentials.Salt) != credentials.PasswordHash)
            {
                return StatusCode(401, new ErrorResponse
                {
                    Error = ErrorCode.InvalidGrant,
                    ErrorDescription = "Invalid password.",
                });
            }
            

            return await GetTokenResponse(new UserData
            {
                Id = credentials.Id,
                Email = credentials.Email,
            });
        }

        /// <summary>
        /// Updates an Access Token.
        /// </summary>
        /// <param name="model">The <see cref="RefreshAccessTokenRequest"/> model.</param>
        /// <returns></returns>
        private async Task<IActionResult> PostRefreshToken([FromBody] RefreshAccessTokenRequest model)
        {
            if (model.GrantType != GrantType.RefreshToken)
                throw new ArgumentException($"Expected '{GrantType.RefreshToken}' value.", nameof(model.GrantType));

            if (String.IsNullOrWhiteSpace(model.RefreshToken))
                return BadRequest(new ErrorResponse
                {
                    Error = ErrorCode.InvalidRequest,
                    ErrorDescription = $"Request was missing the '{nameof(model.RefreshToken)}' parameter.",
                });


            var token = await _refreshTokenRepository.GetByTokenAsync(model.RefreshToken);

            if (token == null)
                return BadRequest(new ErrorResponse
                {
                    Error = ErrorCode.InvalidGrant,
                    ErrorDescription = "Invalid refresh token.",
                });

            if (token.Expires < DateTime.UtcNow)
                return BadRequest(new ErrorResponse
                {
                    Error = ErrorCode.InvalidGrant,
                    ErrorDescription = "Refresh token has expired.",
                });


            token.Expires = DateTime.MinValue;
            await _refreshTokenRepository.UpdateAsync(token);

            var user = await _userRepository.GetByIdAsync(token.UserId);

            return await GetTokenResponse(new UserData
            {
                Id = user.Id,
                Email = user.Email,
            });
        }

        private async Task<IActionResult> GetTokenResponse(UserData user)
        {
            var identity = GetIdentity(user);

            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                issuer: _authSettings.Issuer,
                audience: _authSettings.Audience,
                claims: identity.Claims,
                notBefore: now,
                expires: now.Add(_authSettings.AccessTokenLifetime),
                signingCredentials: new SigningCredentials(AuthSettings.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var refreshToken = Guid.NewGuid().ToString().Replace("-", "");
            var token = new RefreshToken
            {
                Token = refreshToken,
                Expires = now.Add(_authSettings.RefreshTokenLifetime),
                CreatedAt = now,
                UserId = user.Id,
            };
            await _refreshTokenRepository.AddAsync(token);

            return Json(new AccessTokenResponse
            {
                AccessToken = encodedJwt,
                ExpiresIn = (long)_authSettings.AccessTokenLifetime.TotalSeconds,
                RefreshToken = refreshToken,
                UserId = user.Id,
            });
        }

        private ClaimsIdentity GetIdentity(UserData user)
        {
            var claims = new List<Claim>
            {
                new Claim("id", user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, "user"),
            };

            return new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);
        }
    }
}