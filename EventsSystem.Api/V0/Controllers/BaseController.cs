﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.V0.Models.ViewModels.User;
using EventsSystem.Services;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace EventsSystem.Api.V0.Controllers
{
    public abstract class BaseController : Controller
    {
        private IUserService _usersService;

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            _usersService = context.HttpContext.RequestServices.GetService<IUserService>();
        }

        protected UserVm CurrentUser
        {
            get
            {
                var task = _usersService.GetByEmailAsync(User?.Identity?.Name);
                task.Wait();
                var userDto = task.Result;
                return Mapper.Map<UserVm>(userDto.Data);
            }
        }

        protected T GetService<T>()
        {
            return HttpContext.RequestServices.GetService<T>();
        }

        protected IActionResult GetErrorResult(Result result)
        {
            if (result == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            if (result.ContainsError(ErrorType.NotUnique))
            {
                return StatusCode(StatusCodes.Status409Conflict);
            }
            if (result.ContainsError(ErrorType.NotFound))
            {
                return NotFound();
            }
            if (result.HasErrors)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}