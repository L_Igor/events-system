﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.Core.ActionFilters;
using EventsSystem.Api.V0.Models.ViewModels.Event;
using EventsSystem.Services.Dto.Event;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST event service.
    /// </summary>
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class EventsController : BaseController
    {
        private readonly IEventService _eventService;

        private const string GetByIdRouteName = nameof(V0) + nameof(EventsController) + "GetById";

        /// <summary>
        /// Initializes a new instance of the <see cref="EventsController" /> class.
        /// </summary>
        /// <param name="eventService">The event service.</param>
        public EventsController(IEventService eventService)
        {
            _eventService = eventService;
        }

        /// <summary>
        /// Gets all event.
        /// </summary>
        /// <returns>All available event.</returns>
        /// <response code="200">The successfully retrieved event.</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<EventVm>), 200)]
        public async Task<IActionResult> Get()
        {
            var result = await _eventService.GetAllAsync();
            var eventsDto = result.Data;
            var eventsViewModel = Mapper.Map<IEnumerable<EventVm>>(eventsDto);

            return Ok(eventsViewModel);
        }

        /// <summary>
        /// Gets a single event.
        /// </summary>
        /// <param name="id">The requested event identifier.</param>
        /// <returns>The requested event.</returns>
        /// <response code="200">The event was successfully retrieved.</response>
        /// <response code="404">The event does not exist.</response>
        [HttpGet("{id}", Name = GetByIdRouteName)]
        [ProducesResponseType(typeof(EventVm), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _eventService.GetByIdAsync(id);
            var eventDto = result.Data;
            if (eventDto == null)
            {
                return NotFound();
            }
            var eventViewModel = Mapper.Map<EventVm>(eventDto);

            return Ok(eventViewModel);
        }

        /// <summary>
        /// Creates a new event.
        /// </summary>
        /// <param name="model">The event to create.</param>
        /// <returns>The created event.</returns>
        /// <response code="201">The event was successfully created.</response>
        /// <response code="400">The event is invalid.</response>
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(typeof(EventVm), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] CreateEventVm model)
        {
            var createEventDto = Mapper.Map<CreateEventDto>(model);
            var result = await _eventService.CreateAsync(createEventDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var eventDtoResult = await _eventService.GetByIdAsync(result.Data);
            var eventDto = eventDtoResult.Data;
            var eventViewModel = Mapper.Map<EventVm>(eventDto);

            return CreatedAtRoute(GetByIdRouteName, new { id = eventViewModel.Id }, eventViewModel);
        }

        /// <summary>
        /// Modifies an existing event.
        /// </summary>
        /// <param name="id">The modified event identifier.</param>
        /// <param name="model">The event to modify.</param>
        /// <returns>The modified event.</returns>
        /// <response code="200">The event was successfully modified.</response>
        /// <response code="400">The event is invalid.</response>
        /// <response code="404">The event does not exist.</response>
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(typeof(EventVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateEventVm model)
        {
            var updateEventDto = new UpdateEventDto()
            {
                Id = id,
            };
            Mapper.Map<UpdateEventVm, UpdateEventDto>(model, updateEventDto);
            var result = await _eventService.UpdateAsync(updateEventDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var eventDtoResult = await _eventService.GetByIdAsync(id);
            var eventDto = eventDtoResult.Data;
            var eventViewModel = Mapper.Map<EventVm>(eventDto);

            return Ok(eventViewModel);
        }

        /// <summary>
        /// Deletes an existing event.
        /// </summary>
        /// <param name="id">The deleted event identifier.</param>
        /// <returns></returns>
        /// <response code="204">The event was successfully deleted.</response>
        /// <response code="404">The event does not exist.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _eventService.DeleteAsync(id);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }
    }
}