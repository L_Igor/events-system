﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.Core.ActionFilters;
using EventsSystem.Api.V0.Models;
using EventsSystem.Api.V0.Models.ViewModels.QA;
using EventsSystem.Services.Dto.QA;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST question and answer service.
    /// </summary>
    [Authorize]
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class QuestionsController : BaseController
    {
        private readonly IQAService _qaService;

        private const string GetByIdRouteName = nameof(V0) + nameof(QuestionsController) + "GetById";

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionsController" /> class.
        /// </summary>
        /// <param name="qaService">The question and answear service.</param>
        public QuestionsController(IQAService qaService)
        {
            _qaService = qaService;
        }

        /// <summary>
        /// Gets all question and answer.
        /// </summary>
        /// <returns>All available question and answer.</returns>
        /// <response code="200">The successfully retrieved question and answer.</response>
        [AllowAnonymous]
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<QAVm>), 200)]
        public async Task<IActionResult> Get()
        {
            var result = await _qaService.GetAllAsync();
            var qasDto = result.Data;
            var qasViewModel = Mapper.Map<IEnumerable<QAVm>>(qasDto);

            return Ok(qasViewModel);
        }

        /// <summary>
        /// Gets a single question and answer.
        /// </summary>
        /// <param name="id">The requested question and answer identifier.</param>
        /// <returns>The requested question and answer.</returns>
        /// <response code="200">The question and answer was successfully retrieved.</response>
        /// <response code="404">The question and answer does not exist.</response>
        [AllowAnonymous]
        [HttpGet("{id}", Name = GetByIdRouteName)]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _qaService.GetByIdAsync(id);
            var qaDto = result.Data;
            if (qaDto == null)
            {
                return NotFound();
            }
            var qaViewModel = Mapper.Map<QAVm>(qaDto);

            return Ok(qaViewModel);
        }

        /// <summary>
        /// Creates a new question and answer.
        /// </summary>
        /// <param name="model">The question and answer to create.</param>
        /// <returns>The created question and answer.</returns>
        /// <response code="201">The question and answer was successfully created.</response>
        /// <response code="400">The question and answer is invalid.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] CreateQAVm model)
        {
            var createQADto = Mapper.Map<CreateQADto>(model);
            var result = await _qaService.CreateAsync(createQADto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var qaDtoResult = await _qaService.GetByIdAsync(result.Data);
            var qaDto = qaDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(qaDto);

            return CreatedAtRoute(GetByIdRouteName, new { id = qaViewModel.Id }, qaViewModel);
        }

        /// <summary>
        /// Modifies an existing question and answer.
        /// </summary>
        /// <param name="id">The modified question and answer identifier.</param>
        /// <param name="model">The question and answer to modify.</param>
        /// <returns>The modified question and answer.</returns>
        /// <response code="200">The question and answer was successfully modified.</response>
        /// <response code="400">The question and answer is invalid.</response>
        /// <response code="404">The question and answer does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateQAVm model)
        {
            var updateQADto = new UpdateQADto()
            {
                Id = id,
            };
            Mapper.Map<UpdateQAVm, UpdateQADto>(model, updateQADto);
            var result = await _qaService.UpdateAsync(updateQADto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var qaDtoResult = await _qaService.GetByIdAsync(id);
            var qaDto = qaDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(qaDto);

            return Ok(qaViewModel);
        }

        /// <summary>
        /// Deletes an existing question and answer.
        /// </summary>
        /// <param name="id">The deleted question and answer identifier.</param>
        /// <returns></returns>
        /// <response code="204">The question and answer was successfully deleted.</response>
        /// <response code="404">The question and answer does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _qaService.DeleteAsync(id);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }

        /// <summary>
        /// Creates a new answer.
        /// </summary>
        /// <param name="id">The question identifier.</param>
        /// <param name="model">The added answer.</param>
        /// <returns>The created question and answer.</returns>
        /// <response code="200">The answer was successfully created.</response>
        /// <response code="400">The answer is invalid.</response>
        /// <response code="404">The question does not exist.</response>
        [Authorize]
        [HttpPost("{id}/Answers")]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PostAnswers(int id, [FromBody] CreateAnswerVm model)
        {
            var qaService = GetService<IQAService>();

            var createAnswerDto = new CreateAnswerDto()
            {
                QuestionId = id,
                AnsweringId = CurrentUser.Id,
            };
            Mapper.Map<CreateAnswerVm, CreateAnswerDto>(model, createAnswerDto);
            var result = await qaService.CreateAnswerAsync(createAnswerDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var answerDtoResult = await qaService.GetByIdAsync(result.Data);
            var answerDto = answerDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(answerDto);

            return Ok(qaViewModel);
        }

        /// <summary>
        /// Modifies an existing question.
        /// </summary>
        /// <param name="id">The modified question identifier.</param>
        /// <param name="model">The question to modify.</param>
        /// <returns>The modified question and answer.</returns>
        /// <response code="200">The question was successfully modified.</response>
        /// <response code="400">The question is invalid.</response>
        /// <response code="404">The question does not exist.</response>
        [HttpPut("{id}/Question")]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutQuestion(int id, [FromBody] UpdateQuestionVm model)
        {
            var updateQuestionDto = new UpdateQuestionDto()
            {
                Id = id,
            };
            Mapper.Map<UpdateQuestionVm, UpdateQuestionDto>(model, updateQuestionDto);
            var result = await _qaService.UpdateQuestionAsync(updateQuestionDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var qaDtoResult = await _qaService.GetByIdAsync(id);
            var qaDto = qaDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(qaDto);

            return Ok(qaViewModel);
        }

        /// <summary>
        /// Modifies an existing answer.
        /// </summary>
        /// <param name="id">The modified question identifier.</param>
        /// <param name="model">The answer to modify.</param>
        /// <returns>The modified question and answer.</returns>
        /// <response code="200">The answer was successfully modified.</response>
        /// <response code="400">The answer is invalid.</response>
        /// <response code="404">The question does not exist.</response>
        [Authorize]
        [HttpPut("{id}/Answer")]
        [ValidateModel]
        [ProducesResponseType(typeof(QAVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutAnswer(int id, [FromBody] UpdateAnswerVm model)
        {
            var updateAnswerDto = new UpdateAnswerDto()
            {
                Id = id,
            };
            Mapper.Map<UpdateAnswerVm, UpdateAnswerDto>(model, updateAnswerDto);
            var result = await _qaService.UpdateAnswerAsync(updateAnswerDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var qaDtoResult = await _qaService.GetByIdAsync(id);
            var qaDto = qaDtoResult.Data;
            var qaViewModel = Mapper.Map<QAVm>(qaDto);

            return Ok(qaViewModel);
        }
    }
}