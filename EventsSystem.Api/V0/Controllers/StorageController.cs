﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EventsSystem.Api.Core.Extensions;
using EventsSystem.Api.Core.Filters;
using EventsSystem.Api.V0.Models.ViewModels.Storage;
using EventsSystem.Services.Dto.Storage;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST storage service.
    /// </summary>
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public partial class StorageController : Controller
    {
        private readonly IStorageService _storageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="StorageController" /> class.
        /// </summary>
        /// <param name="storageService">The storage service.</param>
        public StorageController(IStorageService storageService)
        {
            _storageService = storageService;
        }

        /// <summary>
        /// Gets a file.
        /// </summary>
        /// <param name="id">The requested file identifier.</param>
        /// <returns>The requested file.</returns>
        /// <response code="200">The file was successfully retrieved.</response>
        /// <response code="404">The file does not exist.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(File), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _storageService.GetByIdAsync(id);
            var storageDto = result.Data;
            if (storageDto == null)
            {
                return NotFound();
            }

            return File(storageDto.Data, storageDto.MimeType);
        }

        /// <summary>
        /// Uploads files to the server.
        /// </summary>
        /// <returns>The uploaded files.</returns>
        /// <response code="200">The files was successfully uploaded.</response>
        [Authorize]
        [HttpPost]
        [DisableFormValueModelBinding]
        [ProducesResponseType(typeof(IEnumerable<FileVm>), 200)]
        public async Task<IActionResult> Post()
        {
            var formDataResult = await Request.StreamFormData();
            var fileIds = new List<int>();

            foreach (var file in formDataResult.Files)
            {
                byte[] data;
                using (var sourceStream = System.IO.File.Open(file.TempFileName, FileMode.Open))
                {
                    using (var destinationStream = new MemoryStream())
                    {
                        await sourceStream.CopyToAsync(destinationStream);
                        data = destinationStream.ToArray();
                    }
                }

                System.IO.File.Delete(file.TempFileName);

                var storageDomain = new CreateStorageDto()
                {
                    Data = data,
                    Length = file.Length,
                    MimeType = file.ContentType,
                    OriginalFileName = file.FileName,
                };

                var result = await _storageService.CreateAsync(storageDomain);
                if (!result.HasErrors)
                {
                    fileIds.Add(result.Data);
                }
            }

            var photosVm = fileIds.Select(id => new FileVm()
            {
                FileId = id,
            });

            return Ok(photosVm);
        }
    }
}
