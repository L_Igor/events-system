﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EventsSystem.Api.Core.ActionFilters;
using EventsSystem.Api.V0.Models;
using EventsSystem.Api.V0.Models.ViewModels.Lecture;
using EventsSystem.Api.V0.Models.ViewModels.User;
using EventsSystem.Services.Dto.User;
using EventsSystem.Services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EventsSystem.Api.V0.Controllers
{
    /// <summary>
    /// Represents a REST user service.
    /// </summary>
    [Authorize]
    [ApiVersion("0.1")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;

        private const string GetByIdRouteName = nameof(V0) + nameof(UsersController) + "GetById";

        /// <summary>
        /// Initializes a new instance of the <see cref="UsersController" /> class.
        /// </summary>
        /// <param name="userService">The user service.</param>
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Gets all user.
        /// </summary>
        /// <returns>All available user.</returns>
        /// <response code="200">The successfully retrieved user.</response>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<UserVm>), 200)]
        public async Task<IActionResult> Get()
        {
            var result = await _userService.GetAllAsync();
            var usersDto = result.Data;
            var usersViewModel = Mapper.Map<IEnumerable<UserVm>>(usersDto);

            return Ok(usersViewModel);
        }

        /// <summary>
        /// Gets a single user.
        /// </summary>
        /// <param name="id">The requested user identifier.</param>
        /// <returns>The requested user.</returns>
        /// <response code="200">The user was successfully retrieved.</response>
        /// <response code="404">The user does not exist.</response>
        [AllowAnonymous]
        [HttpGet("{id}", Name = GetByIdRouteName)]
        [ProducesResponseType(typeof(UserVm), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _userService.GetByIdAsync(id);
            var userDto = result.Data;
            if (userDto == null)
            {
                return NotFound();
            }
            var userViewModel = Mapper.Map<UserVm>(userDto);

            return Ok(userViewModel);
        }

        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <param name="model">The user to create.</param>
        /// <returns>The created user.</returns>
        /// <response code="201">The user was successfully created.</response>
        /// <response code="400">The user is invalid.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(typeof(UserVm), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] CreateUserVm model)
        {
            var createUserDto = Mapper.Map<CreateUserDto>(model);
            var result = await _userService.CreateAsync(createUserDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var userDtoResult = await _userService.GetByIdAsync(result.Data);
            var userDto = userDtoResult.Data;
            var userViewModel = Mapper.Map<UserVm>(userDto);

            return CreatedAtRoute(GetByIdRouteName, new { id = userViewModel.Id }, userViewModel);
        }

        /// <summary>
        /// Modifies an existing user.
        /// </summary>
        /// <param name="id">The modified user identifier.</param>
        /// <param name="model">The user to modify.</param>
        /// <returns>The modified user.</returns>
        /// <response code="200">The user was successfully modified.</response>
        /// <response code="400">The user is invalid.</response>
        /// <response code="404">The user does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpPut("{id}")]
        [ValidateModel]
        [ProducesResponseType(typeof(UserVm), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateUserVm model)
        {
            var updateUserDto = new UpdateUserDto()
            {
                Id = id,
            };
            Mapper.Map<UpdateUserVm, UpdateUserDto>(model, updateUserDto);
            var result = await _userService.UpdateAsync(updateUserDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }
            var userDtoResult = await _userService.GetByIdAsync(id);
            var userDto = userDtoResult.Data;
            var userViewModel = Mapper.Map<UserVm>(userDto);

            return Ok(userViewModel);
        }

        /// <summary>
        /// Deletes an existing user.
        /// </summary>
        /// <param name="id">The deleted user identifier.</param>
        /// <returns></returns>
        /// <response code="204">The user was successfully deleted.</response>
        /// <response code="404">The user does not exist.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _userService.DeleteAsync(id);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a list of existing users.
        /// </summary>
        /// <param name="ids">The list of identifiers.</param>
        /// <returns></returns>
        /// <response code="204">The users was successfully deleted.</response>
        [Authorize(Roles = UserRole.Admin)]
        [HttpDelete]
        [ProducesResponseType(204)]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<int> ids)
        {
            var result = await _userService.DeleteAsync(ids);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return NoContent();
        }

        /// <summary>
        /// Gets all lectures of the specified user.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns>All available user lectures.</returns>
        /// <response code="200">The successfully retrieved user lectures.</response>
        [AllowAnonymous]
        [HttpGet("{id}/Lectures")]
        [ProducesResponseType(typeof(IEnumerable<LectureVm>), 200)]
        public async Task<IActionResult> GetUserLectures(int id)
        {
            var lectureService = GetService<ILectureService>();

            var result = await lectureService.GetUserLecturesAsync(id);
            var lecturesDto = result.Data;
            var lecturesViewModel = Mapper.Map<IEnumerable<LectureVm>>(lecturesDto);

            return Ok(lecturesViewModel);
        }

        /// <summary>
        /// Gets the user photo.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns>The image.</returns>
        /// <response code="200">The photo was successfully retrieved.</response>
        /// <response code="404">The user does not exist.</response>
        [AllowAnonymous]
        [HttpGet("{id}/Avatar")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetAvatar(int id)
        {
            var result = await _userService.GetByIdAsync(id);
            var userDto = result.Data;
            if (userDto == null)
            {
                return NotFound();
            }

            return
                await new StorageController(GetService<IStorageService>())
                    .Get(userDto.PhotoId);
        }

        /// <summary>
        /// Saves the user photo.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <param name="model">The photo.</param>
        /// <response code="200">The photo was successfully saved.</response>
        /// <response code="404">The user does not exist.</response>
        [HttpPut("{id}/Avatar")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutAvatar(int id, [FromBody] SaveUserPhotoVm model)
        {
            var appSettings = GetService<Microsoft.Extensions.Options.IOptions<Settings.AppSettings>>().Value;
            var photoDto = new SaveUserPhotoDto()
            {
                Id = id,
                Photo = UrlCombine(appSettings.DomainUrl, Url.Action(nameof(GetAvatar), new { id = id })),
            };
            Mapper.Map<SaveUserPhotoVm, SaveUserPhotoDto>(model, photoDto);

            var result = await _userService.SavePhoto(photoDto);
            if (result.HasErrors)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        private static string UrlCombine(string url1, string url2)
        {
            if (url1 == null || url2 == null)
                throw new ArgumentNullException(url1 == null ? nameof(url1) : nameof(url2));

            if (url2.Length == 0)
                return url1;

            if (url1.Length == 0)
                return url2;

            url1 = url1.Trim().TrimEnd('/', '\\');
            url2 = url2.Trim().TrimStart('/', '\\');

            return $"{url1}/{url2}";
        }
    }
}