﻿namespace EventsSystem.Api.V0.Models.Auth
{
    public class UserData
    {
	    public int Id { get; set; }

	    public string Email { get; set; }

	    public string FullName { get; set; }
	}
}
