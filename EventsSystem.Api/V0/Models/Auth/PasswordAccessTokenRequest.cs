﻿using Newtonsoft.Json;

namespace EventsSystem.Api.V0.Models.Auth
{
    public class PasswordAccessTokenRequest : AccessTokenRequest
    {
        /// <summary>
        /// Gets or sets the user’s username.
        /// </summary>
        [JsonProperty("username")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the user’s password.
        /// </summary>
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
