﻿using Newtonsoft.Json;

namespace EventsSystem.Api.V0.Models.Auth
{
    /// <summary>
    /// Represents the access token response.
    /// </summary>
    public class AccessTokenResponse
    {
        /// <summary>
        /// Gets the type of the token issued.
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType => "Bearer";

        /// <summary>
        /// Gets or sets the access token issued by the authorization server.
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the lifetime in seconds of the access token.
        /// </summary>
        [JsonProperty("expires_in")]
        public long? ExpiresIn { get; set; }

        /// <summary>
        /// Gets or sets the refresh token which can use to obtain another access token.
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        [JsonProperty("user_id")]
        public long UserId { get; set; }
    }
}
