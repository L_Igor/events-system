﻿using Newtonsoft.Json;
using EventsSystem.Core.JsonConverters;
using EventsSystem.Core.Attributes;

namespace EventsSystem.Api.V0.Models.Auth
{
    /// <summary>
    /// Represents the access token request.
    /// </summary>
    [JsonConverter(typeof(ImplementationTypeConverter))]
    [Implementation(PropertyName = "grant_type", Value = "password", MappingType = typeof(PasswordAccessTokenRequest))]
    [Implementation(PropertyName = "grant_type", Value = "refresh_token", MappingType = typeof(RefreshAccessTokenRequest))]
    public class AccessTokenRequest
    {
        /// <summary>
        /// Gets or sets the grant type.
        /// </summary>
        [JsonProperty("grant_type")]
        public string GrantType { get; set; }
    }
}
