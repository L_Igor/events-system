﻿using Newtonsoft.Json;

namespace EventsSystem.Api.V0.Models.Auth
{
    /// <summary>
    /// Represents the error response.
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the text providing additional information.
        /// </summary>
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }
}
