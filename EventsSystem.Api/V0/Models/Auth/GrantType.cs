﻿namespace EventsSystem.Api.V0.Models.Auth
{
    public class GrantType
    {
        public const string AuthorizationCode = "authorization_code";
        public const string ClientCredentials = "client_credentials";
        public const string Password = "password";
        public const string RefreshToken = "refresh_token";
    }
}
