﻿using Newtonsoft.Json;

namespace EventsSystem.Api.V0.Models.Auth
{
    public class RefreshAccessTokenRequest : AccessTokenRequest
    {
        /// <summary>
        /// Gets or sets the refresh token previously issued to the client.
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
    }
}
