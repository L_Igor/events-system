﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.LectureRating
{
    /// <summary>
    /// Represents a lecture rating.
    /// </summary>
    public class CreateLectureRatingVm
    {
        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        [Required]
        [Range(1, 5)]
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets what the user likes.
        /// </summary>
        [StringLength(256)]
        public string Advantages { get; set; }

        /// <summary>
        /// Gets or sets what the user did not like.
        /// </summary>
        [StringLength(256)]
        public string Disadvantages { get; set; }

        /// <summary>
        /// Gets or sets additional information.
        /// </summary>
        [StringLength(512)]
        public string AdditionalInfo { get; set; }
    }
}
