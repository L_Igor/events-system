﻿namespace EventsSystem.Api.V0.Models.ViewModels.LectureRating
{
    /// <summary>
    /// Represents a lecture rating.
    /// </summary>
    public class LectureRatingVm
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the rating.
        /// </summary>
        public int Rating { get; set; }

        /// <summary>
        /// Gets or sets what the user likes.
        /// </summary>
        public string Advantages { get; set; }

        /// <summary>
        /// Gets or sets what the user did not like.
        /// </summary>
        public string Disadvantages { get; set; }

        /// <summary>
        /// Gets or sets additional information.
        /// </summary>
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Gets or sets the lecture identifier.
        /// </summary>
        public int LectureId { get; set; }

        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        public int UserId { get; set; }
    }
}
