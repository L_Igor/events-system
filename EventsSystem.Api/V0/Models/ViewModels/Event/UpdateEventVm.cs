﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.Event
{
    /// <summary>
    /// Represents an event.
    /// </summary>
    public class UpdateEventVm
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the summary.
        /// </summary>
        [StringLength(256)]
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the event was started.
        /// </summary>
        [Required]
        public DateTimeOffset? StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the event was ended.
        /// </summary>
        [Required]
        public DateTimeOffset? EndDateTime { get; set; }
    }
}
