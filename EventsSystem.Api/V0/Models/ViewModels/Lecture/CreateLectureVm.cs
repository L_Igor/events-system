﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.Lecture
{
    /// <summary>
    /// Represents a lecture.
    /// </summary>
    public class CreateLectureVm
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [StringLength(256)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the lecture was started.
        /// </summary>
        [Required]
        public DateTimeOffset? StartDateTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time when the lecture was ended.
        /// </summary>
        public DateTimeOffset? EndDateTime { get; set; }

        /// <summary>
        /// Gets or sets the event identifier.
        /// </summary>
        [Required]
        public int EventId { get; set; }

        /// <summary>
        /// Gets or sets the speaker identifier.
        /// </summary>
        [Required]
        public int UserId { get; set; }
    }
}
