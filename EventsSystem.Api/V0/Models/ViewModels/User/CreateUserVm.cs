﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.User
{
    /// <summary>
    /// Represents a user.
    /// </summary>
    public class CreateUserVm
    {
        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        [Required]
        [StringLength(256)]
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the bio.
        /// </summary>
        [StringLength(256)]
        public string Bio { get; set; }

        /// <summary>
        /// Gets or sets the occupation.
        /// </summary>
        [StringLength(64)]
        public string Occupation { get; set; }

        /// <summary>
        /// Gets or sets the photo path.
        /// </summary>
        [StringLength(256)]
        public string Photo { get; set; }
    }
}
