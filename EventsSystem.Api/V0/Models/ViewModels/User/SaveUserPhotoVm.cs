﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.User
{
    /// <summary>
    /// Represents a user photo.
    /// </summary>
    public class SaveUserPhotoVm
    {
        /// <summary>
        /// Gets or sets the user photo identifier.
        /// </summary>
        [Required]
        public int PhotoId { get; set; }
    }
}
