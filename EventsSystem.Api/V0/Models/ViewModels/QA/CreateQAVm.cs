﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.QA
{
    /// <summary>
    /// Represents a question and answer.
    /// </summary>
    public class CreateQAVm
    {
        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        [StringLength(256, MinimumLength = 3)]
        public string Answer { get; set; }

        /// <summary>
        /// Gets or sets the user asking identifier.
        /// </summary>
        [Required]
        public int AskingId { get; set; }

        /// <summary>
        /// Gets or sets the user answering identifier.
        /// </summary>
        public int? AnsweringId { get; set; }

        /// <summary>
        /// Gets or sets the lecture identifier.
        /// </summary>
        [Required]
        public int LectureId { get; set; }
    }
}
