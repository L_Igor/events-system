﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.QA
{
    /// <summary>
    /// Represents an answer.
    /// </summary>
    public class CreateAnswerVm
    {
        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Answer { get; set; }
    }
}
