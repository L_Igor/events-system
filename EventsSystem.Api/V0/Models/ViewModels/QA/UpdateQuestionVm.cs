﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.QA
{
    /// <summary>
    /// Represents a question.
    /// </summary>
    public class UpdateQuestionVm
    {
        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        [Required]
        [StringLength(256, MinimumLength = 3)]
        public string Question { get; set; }
    }
}
