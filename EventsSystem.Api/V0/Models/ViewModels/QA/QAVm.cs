﻿using System;

namespace EventsSystem.Api.V0.Models.ViewModels.QA
{
    /// <summary>
    /// Represents a question and answer.
    /// </summary>
    public class QAVm
    {
        /// <summary>
        /// Gets or sets the unique identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Gets or sets the answer.
        /// </summary>
        public string Answer { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the question.
        /// </summary>
        public DateTimeOffset QuestionDateTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time of the answer.
        /// </summary>
        public DateTimeOffset? AnswerDateTime { get; set; }

        /// <summary>
        /// Gets or sets the user asking identifier.
        /// </summary>
        public int AskingId { get; set; }

        /// <summary>
        /// Gets or sets the user answering identifier.
        /// </summary>
        public int? AnsweringId { get; set; }

        /// <summary>
        /// Gets or sets the lecture identifier.
        /// </summary>
        public int LectureId { get; set; }
    }
}
