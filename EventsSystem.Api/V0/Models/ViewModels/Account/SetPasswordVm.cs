﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.Account
{
    /// <summary>
    /// Represents a model for setting the user password.
    /// </summary>
    public class SetPasswordVm
    {
        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string Password { get; set; }
    }
}