﻿using System.ComponentModel.DataAnnotations;

namespace EventsSystem.Api.V0.Models.ViewModels.Account
{
    /// <summary>
    /// Represents a model for changing the user password.
    /// </summary>
    public class ChangePasswordVm
    {
        /// <summary>
        /// Gets or sets the current password.
        /// </summary>
        [Required]
        public string CurrentPassword { get; set; }

        /// <summary>
        /// Gets or sets the new password.
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength = 6)]
        public string NewPassword { get; set; }
    }
}