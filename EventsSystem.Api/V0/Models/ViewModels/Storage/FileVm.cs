﻿namespace EventsSystem.Api.V0.Models.ViewModels.Storage
{
    /// <summary>
    /// Represents a file.
    /// </summary>
    public class FileVm
    {
        /// <summary>
        /// Gets or sets the file identifier.
        /// </summary>
        public int FileId { get; set; }
    }
}
