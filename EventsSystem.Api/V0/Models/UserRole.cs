﻿namespace EventsSystem.Api.V0.Models
{
    public class UserRole
    {
        public const string Admin = null;
        public const string User = "user";
        public const string Lecturer = "lecturer";
    }
}
