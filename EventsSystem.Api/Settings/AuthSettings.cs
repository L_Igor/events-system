﻿using System;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace EventsSystem.Api.Settings
{
    public class AuthSettings
    {
        private const string Key = "dkXnVh6k6YSmGZv61EaQdZ7Bs7IiTIoIgi9HMsopoID5rzftcLe5IDNb3BKG";

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public TimeSpan AccessTokenLifetime { get; set; }

        public TimeSpan RefreshTokenLifetime { get; set; }

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
