﻿using System.Linq;
using EventsSystem.Core.Helpers;
using EventsSystem.Data.Repository;

namespace EventsSystem.Api.Internal
{
    public class IdentityValidator
    {
        private readonly IUserRepository _userRepository;

        public IdentityValidator(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool Validate(string userName, string password)
        {
            var credentials =
                _userRepository.Query()
                    .FirstOrDefault(x => x.UserName.Equals(userName));

            if (credentials != null &&
                AuthHelper.GetPassword(password, credentials.Salt) == credentials.PasswordHash)
            {
                return true;
            }

            return false;
        }
    }
}
