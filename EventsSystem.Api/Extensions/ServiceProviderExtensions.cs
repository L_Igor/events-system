﻿using EventsSystem.Api.Core.Authentication;
using EventsSystem.Api.Internal;
using EventsSystem.Api.Settings;
using EventsSystem.Data.Infrastructure;
using EventsSystem.Data.Infrastructure.Impl;
using EventsSystem.Data.Repository;
using EventsSystem.Data.Repository.Impl;
using EventsSystem.Data.Settings;
using EventsSystem.Services.Services;
using EventsSystem.Services.Services.Impl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EventsSystem.Api.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static void RegisterDependencies(this IServiceCollection services)
        {
            services.AddScoped(factory => new ApplicationBasicAuthenticationEvents()
            {
                OnValidateCredentials = new IdentityValidator(factory.GetRequiredService<IUserRepository>()).Validate,
            });

            services.AddTransient<IContextFactory, DefaultContextFactory>();

            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<ILectureRepository, LectureRepository>();
            services.AddTransient<ILectureRatingRepository, LectureRatingRepository>();
            services.AddTransient<IQARepository, QARepository>();
            services.AddTransient<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddTransient<IStorageRepository, StorageRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<IEventService, EventService>();
            services.AddTransient<ILectureService, LectureService>();
            services.AddTransient<IQAService, QAService>();
            services.AddTransient<IStorageService, StorageService>();
            services.AddTransient<IUserService, UserService>();
        }

        public static void RegisterOptions(this IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddOptions();

            services.Configure<AppSettings>(configuration.GetSection("App"));
            services.Configure<AuthSettings>(configuration.GetSection("Auth"));
            services.Configure<DatabaseSettings>(configuration.GetSection("Database"));
        }
    }
}