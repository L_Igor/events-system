using System;
using System.Linq;
using System.Reflection;
using EventsSystem.Core.Attributes;
using EventsSystem.Core.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace EventsSystem.Core.JsonConverters
{
    public class ImplementationTypeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            var attributes = objectType.GetCustomAttributes<ImplementationAttribute>();
            if (!attributes.Any())
                return false;

            var properties = objectType
                .GetProperties(BindingFlags.Public | BindingFlags.GetProperty | BindingFlags.Instance)
                .Select(a => a.Name);

            return attributes.All(a => properties.Contains(a.PropertyName));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);

            foreach (var attribute in objectType.GetCustomAttributes<ImplementationAttribute>())
            {
                var jObjectValue = jObject.GetValue(attribute.PropertyName, StringComparison.InvariantCultureIgnoreCase)?.Value<string>();
                if (!String.Equals(jObjectValue, attribute.Value, StringComparison.InvariantCultureIgnoreCase))
                    continue;

                var target = Activator.CreateInstance(attribute.MappingType);
                serializer.Populate(jObject.CreateReader(), target);

                return target;
            }

            var result = Activator.CreateInstance(objectType);
            serializer.Populate(jObject.CreateReader(), result);

            return result;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var jObject = new JObject();
            var type = value.GetType();

            var resolver = serializer.ContractResolver as DefaultContractResolver;

            foreach (var propertyInfo in type.GetProperties())
            {
                if (propertyInfo.CanRead)
                {
                    var propVal = propertyInfo.GetValue(value, null);
                    if (propVal != null)
                    {
                        var propName = (resolver != null)
                            ? resolver.GetResolvedPropertyName(propertyInfo.Name)
                            : propertyInfo.Name;
                        jObject.Add(propName, JToken.FromObject(propVal, serializer));
                    }
                }
            }
            jObject.WriteTo(writer);
        }
    }
}