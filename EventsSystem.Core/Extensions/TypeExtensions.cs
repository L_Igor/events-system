﻿using System;

namespace EventsSystem.Core.Extensions
{
    public static class TypeExtensions
    {
        public static T[] GetCustomAttributes<T>(this Type type)
            where T : Attribute
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            return Attribute.GetCustomAttributes(type, typeof(T)) as T[];
        }
    }
}
