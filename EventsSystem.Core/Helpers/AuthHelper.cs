﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace EventsSystem.Core.Helpers
{
    public class AuthHelper
    {
        private static readonly Random Random = new Random();

        public static string CreateSalt(int length = 32)
        {
            const string chars = @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+/\;:,.?";
            return new string(Enumerable.Range(0, length)
                .Select(i => chars[Random.Next(chars.Length)])
                .ToArray());
        }

        public static string GetPassword(string password, string salt)
        {
            using (var sha1 = SHA1.Create())
            {
                var bytes = sha1.ComputeHash(Encoding.Unicode.GetBytes(password));
                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }

        public static string GetPassword(string password, out string salt)
        {
            salt = CreateSalt();
            return GetPassword(password, salt);
        }
    }
}
