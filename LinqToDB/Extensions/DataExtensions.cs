﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Mapping;

namespace LinqToDB.Extensions
{
    public static class DataExtensions
    {
        public static T GetById<T>(this IDataContext dataContext, object id)
            where T : class
        {
            var pkPropertyInfo = GetPkPropertyInfo(typeof(T));
            var expression = SimpleComparison<T>(pkPropertyInfo.Name, id);

            return dataContext.GetTable<T>().FirstOrDefault(expression);
        }

        public static async Task<T> GetByIdAsync<T>(this IDataContext dataContext, object id)
            where T : class
        {
            var pkPropertyInfo = GetPkPropertyInfo(typeof(T));
            var expression = SimpleComparison<T>(pkPropertyInfo.Name, id);

            return await dataContext.GetTable<T>().FirstOrDefaultAsync(expression);
        }

        public static void InsertAndSetIdentity<T>(this IDataContext dataContext, T obj)
        {
            var identityValue = dataContext.InsertWithIdentity<T>(obj);
            obj.SetIdentityValue(identityValue);
        }

        public static async Task InsertAndSetIdentityAsync<T>(this IDataContext dataContext, T obj)
        {
            var identityValue = await dataContext.InsertWithIdentityAsync<T>(obj);
            obj.SetIdentityValue(identityValue);
        }

        private static void SetIdentityValue<T>(this T obj, object value)
        {
            var pkPropertyInfo = GetPkPropertyInfo(typeof(T));
            pkPropertyInfo.SetValue(obj, Convert.ChangeType(value, pkPropertyInfo.PropertyType), null);
        }

        private static PropertyInfo GetPkPropertyInfo(Type type)
        {
            var pkPropertyInfo = type.GetProperties().First(p => p.GetCustomAttributes(typeof(PrimaryKeyAttribute), false).Any());
            return pkPropertyInfo;
        }

        private static Expression<Func<T, bool>> SimpleComparison<T>(string property, object value)
        {
            var type = typeof(T);
            var parameterExpression = Expression.Parameter(type, "p");
            var propertyReference = Expression.Property(parameterExpression, property);
            var constantReference = Expression.Constant(value);

            return Expression.Lambda<Func<T, bool>>(Expression.Equal(propertyReference, constantReference), new[] { parameterExpression });
        }
    }
}